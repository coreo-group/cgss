#!/usr/bin/env python
#-*- coding:utf-8 -*-
##
## loandra.py
##
##  Created on: Feb 11, 2021
##      Author: Jeremias Berg
##      E-mail: jeremias.berg@helsinki.fi
##


#==============================================================================
from __future__ import print_function
import collections
import getopt
import itertools
from math import copysign
import os
from pysat.formula import CNFPlus, WCNFPlus
from pysat.card import ITotalizer, ITotalizerLazier, SSEncoder
from pysat.pb import IGENTotalizer
import pysat.exch
from pysat.solvers import Solver, SolverNames
import re
import six
from six.moves import range
import sys
import count
import random
import time
import gc
#gc.set_debug(gc.DEBUG_STATS)

def check_cost(lits=[], costs=[], model=[]) :
  assert (len(lits) == len(costs))
  cost = 0
  for (l, c) in zip(lits, costs) : 
    if l in model: cost += c 
  return cost


if __name__ == '__main__':
  for j in range(10):
    print("################# START TEST {:n}##################".format(j))
    n=random.randint(50, 100)
    lits = list(range(1,n+1))
    weights = random.sample(range(1, 100), n)
    u_bound = sum(weights)
    print("#Lits, Weights ", n)
    print("Bounds: ", u_bound)
    #print(t.cnf.clauses)
    #print(t.rhs)
    t = IGENTotalizer(lits=lits, weights=weights, ubound=u_bound)
    s = Solver(name="g3", bootstrap_with=t.cnf.clauses, incr=False, use_timer=True)
    print("Totalizer built")
    for bound in t.weights:
      i = 0
      result = 1 
      ass_lit = t.bound_literal(bound)
      print ("######### NEW BOUND ##############")
      print ("Bound: ", bound)
      print ("Assumption literal: ", ass_lit)
      while result:
        i += 1
        print("##################################")
        print("ITERATION: {:n}".format(i))
        result = s.solve(assumptions=[ -ass_lit])

        if result:
          c = check_cost(lits, weights, s.get_model())
          if c < bound:
            print("OK")
          else :
            print("ERROR")
            print("Model lits: \t", [x for x in s.get_model() if abs(x) in lits])
            print("Weights: \t", weights)
            print("Cost: \t", c)
            print("Bound: \t", bound)
          block = [-x for x in s.get_model() if abs(x) in lits]
          s.add_clause(block)
          print("##################################")
        else : 
          print("UNSAT")
          print("###########BOUND DONE#############")
          print("##################################")
          print()
    s.delete()
    t.delete()
    print("#################END TEST##################")
    print("###########################################")
    print()

