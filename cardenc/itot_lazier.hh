/*
 * itot_lazier.hh, copied from itot.hh
 * 
 * itot.hh
 *
 *  Created on: May 29, 2015
 *      Author: Antonio Morgado, Alexey S. Ignatiev
 *      E-mail: {ajmorgado,aignatiev}@ciencias.ulisboa.pt
 */

#ifndef ITOT_LAZIER_HH_
#define ITOT_LAZIER_HH_

#include <algorithm>
#include <cmath>
#include <vector>
#include <deque>
#include "clset.hh"

using namespace std;

typedef struct TotTreeL {
    vector<int> vars;
    unsigned space;
    unsigned nof_input;
    TotTreeL *left;
    TotTreeL *right;
} TotTreeL;

//
//=============================================================================
void itot_lazier_new_ua(
    int& top,
    ClauseSet& dest,
    vector<int>& ov,
    unsigned rhs,
    vector<int>& av,
    vector<int>& bv
)
{
    // i = 0
    unsigned kmin = std::min(rhs, (unsigned)bv.size());
    for (unsigned j = 0; j < kmin; ++j)
        dest.create_binary_clause(-bv[j], ov[j]);

    // j = 0
    kmin = std::min(rhs, (unsigned)av.size());
    for (unsigned i = 0; i < kmin; ++i)
        dest.create_binary_clause(-av[i], ov[i]);

    // i, j > 0
    for (unsigned i = 1; i <= kmin; ++i) {
        unsigned minj = std::min(rhs - i, (unsigned)bv.size());
        for (unsigned j = 1; j <= minj; ++j)
            dest.create_ternary_clause(-av[i - 1], -bv[j - 1], ov[i + j - 1]);
    }
}

//
//=============================================================================
TotTreeL *itot_lazier_new(ClauseSet& dest, vector<int>& lhs, unsigned rhs, unsigned size, int& top) {
    unsigned n = lhs.size();
    deque<TotTreeL *> nodes;

    for (unsigned i = 0; i < size; ++i) {
        TotTreeL *tree = new TotTreeL();

        tree->left      = 0;
        tree->right     = 0;
        tree->nof_input = 1;
        if (i<n) {
            tree->vars.resize(1);
            tree->vars[0]   = lhs[i];
            tree->space     = 0;
        } else {
            tree->nof_input = 1;
            tree->space     = 1;
        }
        
        nodes.push_back(tree);
    }
    

    while (nodes.size()>1) {
        TotTreeL *l = nodes.front();
        nodes.pop_front();
        TotTreeL *r = nodes.front();
        nodes.pop_front();

        TotTreeL *p = new TotTreeL();
        nodes.push_back(p);

        p->left         = l;
        p->right        = r;
        p->nof_input    = l->nof_input + r->nof_input;
        p->space        = l->space + r->space;
        

        if (p->space < p->nof_input || nodes.size()==1) {
            unsigned kmin = min(rhs, p->nof_input - p->space); // itot.hh had rhs + 1, but it is nicer to have itot code without weird +1. This affects what rhs value means and that must be handled in somewhere else.
            if (nodes.size()==1) {
                // root is allowed to have one extra output variable: the next non-falsified rhs may be needed even when it cannot yet be falsified
                // also root must always have at least 2 output variables
                kmin=max(2u,min(rhs, p->nof_input - (p->space > 0 ? p->space-1:0)));
            }
            p->vars.resize(kmin);
            for (unsigned i = 0; i < kmin; ++i) p->vars[i] = ++top;

            itot_lazier_new_ua(top, dest, p->vars, kmin, l->vars, r->vars);
        }
	}

	return nodes.front();
}

//
//=============================================================================
void itot_lazier_increase_ua(
    int& top,
    ClauseSet& dest,
    vector<int>& ov,
    vector<int>& av,
    vector<int>& bv,
    unsigned rhs
)
{
    unsigned last = ov.size();

    for (unsigned i = last; i < rhs; ++i)
        ov.push_back(++top);

    // add the constraints
    // i = 0
    unsigned maxj = std::min(rhs, (unsigned)bv.size());
    for (unsigned j = last; j < maxj; ++j)
        dest.create_binary_clause(-bv[j], ov[j]);

    // j = 0
    unsigned maxi = std::min(rhs, (unsigned)av.size());
    for (unsigned i = last; i < maxi; ++i)
        dest.create_binary_clause(-av[i], ov[i]);

    // i, j > 0
    for (unsigned i = 1; i <= maxi; ++i) {
        unsigned maxj = std::min(rhs - i, (unsigned)bv.size());
        unsigned minj = std::max((int)last - (int)i + 1, 1);
        for (unsigned j = minj; j <= maxj; ++j)
            dest.create_ternary_clause(-av[i - 1], -bv[j - 1], ov[i + j - 1]);
    }
}

//
//=============================================================================
void itot_lazier_increase_(TotTreeL *tree, ClauseSet& dest, unsigned rhs, int& top)
{
    unsigned kmin = std::min(rhs, tree->nof_input - tree->space);

    if (kmin <= tree->vars.size())
        return;

    itot_lazier_increase_  (tree->left,  dest, rhs, top);
    itot_lazier_increase_  (tree->right, dest, rhs, top);
    itot_lazier_increase_ua(top, dest, tree->vars, tree->left->vars, tree->right->vars, kmin);
}

void itot_lazier_increase(TotTreeL *tree, ClauseSet& dest, unsigned rhs, int& top)
{
    unsigned kmin = std::min(rhs, tree->nof_input - (tree->space>0?tree->space-1:0)); // root node is allowed to have one extra output variable - because sum < k may be falsified with k input variables, we may need sum < k+1 when the number of input variables is only k

    if (kmin <= tree->vars.size())
        return;

    itot_lazier_increase_  (tree->left,  dest, rhs, top);
    itot_lazier_increase_  (tree->right, dest, rhs, top);
    itot_lazier_increase_ua(top, dest, tree->vars, tree->left->vars, tree->right->vars, kmin);
}


void itot_lazier_push_ua(
    int& top,
    ClauseSet& dest,
    vector<int>& ov,
    vector<int>& av, // new variable is assumed to be added in av
    vector<int>& bv
)
{
    dest.create_binary_clause(-av.back(), ov[av.size()-1]);
    
    unsigned maxj = min(ov.size() - av.size(), bv.size());
    for (unsigned j = 1; j <= maxj; ++j) {
        dest.create_ternary_clause(-av.back(), -bv[j - 1], ov[av.size() + j - 1]);
    }
}

// Activate a new variable for which space was reserved on creation
// returns true if node's number of vars changed and thus parents vars should be updated
bool itot_lazier_push(TotTreeL* tree, ClauseSet& dest, int lit, unsigned rhs, int& top) {
    if (!tree->space) return 0; // should actually never happen, unless more than reserved number of lits are tried to activate
    
    if (tree->left && tree->left->space) {
        bool rv=0;
        --tree->space;
        if (tree->vars.size() < min(rhs, tree->nof_input - tree->space)) rv=1, tree->vars.push_back(++top);
        if (itot_lazier_push(tree->left, dest, lit, rhs, top)) itot_lazier_push_ua(top, dest, tree->vars, tree->left->vars, tree->right->vars);
        return rv;
    }else if (tree->right && tree->right->space) {
        bool rv=0;
        --tree->space;
        if (tree->vars.size() < min(rhs, tree->nof_input - tree->space)) rv=1, tree->vars.push_back(++top);
        if (itot_lazier_push(tree->right, dest, lit, rhs, top)) itot_lazier_push_ua(top, dest, tree->vars, tree->right->vars, tree->left->vars);
        return rv;
    } else {
        tree->vars.push_back(lit);
        tree->space = 0;
        return 1;
    }
    return 0;
}
//
//=============================================================================
void itot_lazier_activate(
    TotTreeL *tree,
    ClauseSet& dest,
    vector<int>& lits,
    int& top
)
{
    for (unsigned i=0;i<lits.size();++i) itot_lazier_push(tree, dest, lits[i], tree->vars.size(), top);
}

// recursive destruction of the tree
//=============================================================================
static void itot_lazier_destroy(TotTreeL *tree)
{
    if (tree->left)  itot_lazier_destroy(tree->left);
    if (tree->right) itot_lazier_destroy(tree->right);

    tree->vars.clear(); // this line probably doesn't do anything useful...
    delete tree;
}

#endif // ITOT_HH_
