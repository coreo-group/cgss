/*
 * pycard.cc
 *
 *  Created on: Sep 25, 2017
 *      Author: Alexey S. Ignatiev
 *      E-mail: aignatiev@ciencias.ulisboa.pt
 */

#define PY_SSIZE_T_CLEAN

#include <setjmp.h>
#include <signal.h>
#include <stdio.h>
#include <Python.h>

#include "preprocessorinterface.hpp"

using namespace std;

// docstrings
//=============================================================================
static char    module_docstring[] = "This module provides a python interface for "
                                    "MaxPre, a MaxSAT preprocessor by Tuukka Korhonen.";
static char preprocess_docstring[]  = "Preprocess.";
static char reconstruct_docstring[] = "Reconstruct.";

static PyObject *PyMaxPreError;
static jmp_buf env;

// function declaration for functions available in module
//=============================================================================
extern "C" {
	static PyObject *py_preprocess   (PyObject *, PyObject *);
}

// module specification
//=============================================================================
static PyMethodDef module_methods[] = {
	{ "preprocess",  py_preprocess,  METH_VARARGS,  preprocess_docstring },
	{ NULL, NULL, 0, NULL }
};

extern "C" {

// signal handler for SIGINT
//=============================================================================
static void sigint_handler(int signum)
{
	longjmp(env, -1);
}


static uint64_t pylong_to_cuint64(PyObject *i_obj) {return PyLong_AsUnsignedLongLong(i_obj);}

#if PY_MAJOR_VERSION >= 3  // for Python3
// PyInt_asLong()
//=============================================================================
static int pyint_to_cint(PyObject *i_obj)
{
	return PyLong_AsLong(i_obj);
}

static uint64_t pyint_to_cuint64(PyObject *i_obj)
{
    return PyLong_AsUnsignedLongLong(i_obj);
}

// PyInt_fromLong()
//=============================================================================
static PyObject *pyint_from_cint(int i)
{
	return PyLong_FromLong(i);
}

// PyCapsule_New()
//=============================================================================
static PyObject *void_to_pyobj(void *ptr)
{
	return PyCapsule_New(ptr, NULL, NULL);
}

// PyCapsule_GetPointer()
//=============================================================================
static void *pyobj_to_void(PyObject *obj)
{
	return PyCapsule_GetPointer(obj, NULL);
}

static const char* pyobj_to_string(PyObject *obj) {
    return PyUnicode_AsUTF8(obj);
}

// PyInt_Check()
//=============================================================================
static int pyint_check(PyObject *i_obj)
{
    return PyLong_Check(i_obj);
}

// module initialization
//=============================================================================
static struct PyModuleDef module_def = {
    PyModuleDef_HEAD_INIT,
    "pymaxpre",   /* m_name */
    module_docstring,  /* m_doc */
    -1,                /* m_size */
    module_methods,    /* m_methods */
    NULL,              /* m_reload */
    NULL,              /* m_traverse */
    NULL,              /* m_clear */
    NULL,              /* m_free */
};

PyMODINIT_FUNC PyInit_pymaxpre(void) {
    PyObject *m = PyModule_Create(&module_def);

    if (m == NULL) return NULL;

    PyMaxPreError = PyErr_NewException((char *)"pymaxpre.error", NULL, NULL);
    Py_INCREF(PyMaxPreError);

    if (PyModule_AddObject(m, "error", PyMaxPreError) < 0) {
        Py_DECREF(PyMaxPreError);
        return NULL;
    }

    return m;
}
#else  // for Python2
// PyInt_asLong()
//=============================================================================
static int pyint_to_cint(PyObject *i_obj)
{
    return PyInt_AsLong(i_obj);
}


static uint64_t pyint_to_cuint64(PyObject *i_obj)
{
    return PyInt_AsUnsignedLongLongMask(i_obj);
}


// PyInt_fromLong()
//=============================================================================
static PyObject *pyint_from_cint(int i)
{
    return PyInt_FromLong(i);
}

// PyCObject_FromVoidPtr()
//=============================================================================
static PyObject *void_to_pyobj(void *ptr)
{
    return PyCObject_FromVoidPtr(ptr, NULL);
}

// PyCObject_AsVoidPtr()
//=============================================================================
static void *pyobj_to_void(PyObject *obj)
{
    return PyCObject_AsVoidPtr(obj);
}

// PyInt_Check()
//=============================================================================
static int pyint_check(PyObject *i_obj)
{
    return PyInt_Check(i_obj);
}


static const char* pyobj_to_string(PyObject *obj) {
    return PyString_AsString(obj);
}


// module initialization
//=============================================================================
PyMODINIT_FUNC initpymaxpre(void)
{
    PyObject *m = Py_InitModule3("pymaxpre", module_methods, module_docstring);

    if (m == NULL) return;

    PyMaxPreError = PyErr_NewException((char *)"pymaxpre.error", NULL, NULL);
    Py_INCREF(PyMaxPreError);
    PyModule_AddObject(m, "error", PyMaxPreError);
}


#endif

// auxiliary function for translating an iterable to a vector<int>
//=============================================================================
static bool pyiter_to_vector(PyObject *obj, vector<int>& vect) {
    PyObject *i_obj = PyObject_GetIter(obj);

    if (i_obj == NULL) {
        PyErr_SetString(PyExc_RuntimeError,
                "Object does not seem to be an iterable.");
        return false;
    }

    PyObject *l_obj;
    while ((l_obj = PyIter_Next(i_obj)) != NULL) {
        if (!pyint_check(l_obj)) {
            Py_DECREF(l_obj);
            Py_DECREF(i_obj);
            PyErr_SetString(PyExc_TypeError, "integer expected");
            return false;
        }

        int l = pyint_to_cint(l_obj);
        Py_DECREF(l_obj);

        if (l == 0) {
            Py_DECREF(i_obj);
            PyErr_SetString(PyExc_ValueError, "non-zero integer expected");
            return false;
        }

        vect.push_back(l);
    }

    Py_DECREF(i_obj);
    return true;
}
// auxiliary function for translating an iterable to a vector<int>
//=============================================================================
static bool pyiter_to_vector_uint64(PyObject *obj, vector<uint64_t>& vect) {
    PyObject *i_obj = PyObject_GetIter(obj);

    if (i_obj == NULL) {
        PyErr_SetString(PyExc_RuntimeError,
                "Object does not seem to be an iterable.");
        return false;
    }

    PyObject *l_obj;
    while ((l_obj = PyIter_Next(i_obj)) != NULL) {
        uint64_t l = pyint_to_cuint64(l_obj);
        Py_DECREF(l_obj);
        vect.push_back(l);
    }

    Py_DECREF(i_obj);
    return true;
}


//
//=============================================================================
static PyObject *py_preprocess(PyObject *self, PyObject *args) {
    PyObject *cl_obj; // clauses
    PyObject *w_obj; // weights
    PyObject *topw_obj; // top weight
    double timelimit;
    int loglevel;
    PyObject *opt_obj; //

    if (!PyArg_ParseTuple(args, "OOOdiO", &cl_obj, &w_obj, &topw_obj, &timelimit, &loglevel, &opt_obj)) return NULL;

    vector<vector<int> > clauses;
    PyObject *i_obj = PyObject_GetIter(cl_obj);
    PyObject *l_obj;
    while ((l_obj = PyIter_Next(i_obj)) != NULL) {
        clauses.push_back(vector<int>());
        if (pyiter_to_vector(l_obj, clauses.back())==false) {
            Py_DECREF(l_obj);
            Py_DECREF(i_obj);
            PyErr_SetString(PyExc_TypeError, "list expected");
            return NULL;
        }
    }
    Py_DECREF(i_obj);
    
    vector<uint64_t> weights;
    pyiter_to_vector_uint64(w_obj, weights);
    
    uint64_t topw = pyint_to_cuint64(topw_obj);
    
    std::string opt_string=pyobj_to_string(opt_obj);
    
    vector<vector<int> > new_clauses;
    vector<uint64_t> new_weights;
    vector<int> labels;
    
    maxPreprocessor::PreprocessorInterface* maxpre = new maxPreprocessor::PreprocessorInterface(clauses, weights, topw);
    maxpre->preprocess(opt_string, loglevel, timelimit);
    maxpre->getInstance(new_clauses, new_weights, labels);
    
    PyObject *ncl_obj = PyList_New(new_clauses.size());
    for (size_t i=0; i<new_clauses.size(); ++i) {
        PyObject *clause = PyList_New(new_clauses[i].size());
        for (size_t j=0; j<new_clauses[i].size(); ++j) {
            PyObject *lit = pyint_from_cint(new_clauses[i][j]);
            PyList_SetItem(clause, j, lit);
        }
        PyList_SetItem(ncl_obj, i, clause);
    }
    
    PyObject *nw_obj = PyList_New(new_clauses.size());
    for (size_t i=0; i<new_weights.size(); ++i) {
        PyObject *weight = PyLong_FromUnsignedLongLong(new_weights[i]);
        PyList_SetItem(nw_obj, i, weight);
    }
    
    PyObject *ret = Py_BuildValue("OOO", void_to_pyobj((void *)maxpre), ncl_obj, nw_obj);
    Py_DECREF(ncl_obj);
    Py_DECREF(nw_obj);
    return ret;
}
}



