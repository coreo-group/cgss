from __future__ import print_function
import os
import zipfile
try:
    from urllib2 import urlopen
    from urllib2 import Request
except ImportError:
    from urllib.request import urlopen
    from urllib.request import Request


maxpreurl="https://github.com/laakeri/maxpre/archive/master.zip"
filename="maxpre.zip"

def download_zip(url, filename, override=False):
    if os.path.exists(filename) and not override:
        print('not downloading {0} since it exists locally'.format(filename))
        return
    response = urlopen(url)
    u = urlopen(response.geturl())
    meta = u.info()
    if (meta.get('Content-Length') and len(meta.get('Content-Length')) > 0 ) or meta.get('Content-type'):
        filesz=-1
        if meta.get('Content-Length') and len(meta.get('Content-Length')) > 0:
            filesz = int(meta.get('Content-Length'))
        if os.path.exists(filename) and os.path.getsize(filename) == filesz:
            print('not downloading {0} since it exists locally'.format(filename))
            return
        print('downloading: {0} ({1} bytes)...'.format(filename, filesz), end=' ')
        with open(filename, 'wb') as fp:
            block_sz = 8192
            while True:
                buff = u.read(block_sz)
                if not buff:
                    break
                fp.write(buff)
        print('done')
    else:
        assert 0, 'something went wrong -- cannot download {0}'.format(filename)


#
#==============================================================================
def extract_zip(filename):
    if os.path.exists(filename[:-4]):
        shutil.rmtree(filename[:-4])

    zp = zipfile.ZipFile(filename, 'r')
    zp.extractall()
    zp.close()
    # ugly but works
    os.system("cp Makefile maxpre-master/src/")



def download(override=False):
    download_zip(maxpreurl, filename, override)
    extract_zip(filename)

def compile():
    os.system("cd maxpre-master && make lib")
