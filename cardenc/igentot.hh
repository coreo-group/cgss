/*
 * igentot.hh
 *
 *  Created on: May 29, 2015
 *      Author: Jeremias Berg
 *      E-mail: jeremias.berg@helsinki.fi
 */

#ifndef IGENTOT_HH_
#define IGENTOT_HH_

#include <algorithm>
#include <iostream>
#include <cmath>
#include <vector>
#include <unordered_set>
#include <unordered_map>
#include <deque>
#include "clset.hh"

using namespace std;

typedef struct GenTotTree {
	unordered_map<unsigned, int> vars;
	vector<unsigned> w;
	unsigned nof_input;
	GenTotTree *left;
	GenTotTree *right;
} GenTotTree;

//
//=============================================================================
void igentot_new_ua(
	int& top,
	ClauseSet& dest,
	unordered_map<unsigned, int> & ov,
	vector<unsigned> & ow,
	unsigned rhs,
	unordered_map<unsigned, int> & av,
	vector<unsigned> & aw,
	unordered_map<unsigned, int> & bv, 
	vector<unsigned> & bw
)
{
	// i = 0
	for (unsigned j = 0; j < bw.size(); ++j) {
		if (bw[j] > rhs) {
			dest.create_binary_clause(-bv[bw[j]], ov[rhs]);
		}
		else {
			dest.create_binary_clause(-bv[bw[j]], ov[bw[j]]);
		}
	}

	for (unsigned j = 0; j < aw.size(); ++j) {
		if (aw[j] > rhs) {
			dest.create_binary_clause(-av[aw[j]], ov[rhs]);
		}
		else {
			dest.create_binary_clause(-av[aw[j]], ov[aw[j]]);
		}
	}

	// i, j > 0
	for (unsigned i = 0; i < bw.size(); ++i) {
		if (bw[i] >= rhs) {
			break;
		}
		for (unsigned j = 0; j < aw.size(); ++j) {
			if (aw[j] >= rhs) {
				break;
			}
			unsigned bound = aw[j] + bw[i];
			if (bound > rhs) {
				dest.create_ternary_clause(-av[aw[j]], -bv[bw[i]], ov[rhs]);
			}
			else {
				dest.create_ternary_clause(-av[aw[j]], -bv[bw[i]], ov[aw[j]+bw[i] ]);	
			}
		}		
	}

	////// Minimum
	for (unsigned i = 0; i < ow.size() -1; ++i) {
		dest.create_binary_clause(ov[ow[i]], -ov[ow[i+1]]);
	}
}


void prep_outputs (vector<unsigned> & l_weights, vector<unsigned> & r_weights, vector<unsigned> & outputs_out, unsigned rhs) {
	unordered_set<unsigned> o_w; 
	vector<unsigned> all_w;

	all_w.insert(all_w.end(), l_weights.begin(), l_weights.end());
	all_w.insert(all_w.end(), r_weights.begin(), r_weights.end());

	////////////// DEBUG /////////////////////////////////////////
	/*
	cout << "l weights: " << endl; 
	for(auto w : l_weights) {
		cout << w << " ";
	}
	cout << endl;

	cout << "r weights: " << endl; 
	for(auto w : r_weights) {
		cout << w << " ";
	}
	cout << endl;

	cout << "a weights: " << endl; 
	for(auto w : all_w) {
		cout << w << " ";
	}
	cout << endl;
	*/
	/////////////////////////////////////////////////////////////

	assert(all_w.size() == l_weights.size() + r_weights.size());
	for (unsigned i = 0; i < all_w.size(); i ++) {
		if (all_w[i] <= rhs) {
			o_w.insert(all_w[i]);
		}
		else  {
			o_w.insert(rhs);
		}
		for (unsigned j=i+1; j < all_w.size(); j++) {
			if (all_w[i] +all_w[j] <= rhs) {
				o_w.insert(all_w[i] +all_w[j]);
			}
			else {
				o_w.insert(rhs);
			}
		}
	}
	assert(outputs_out.size() == 0);
	
	/////////////////////////////////////////////////////////////
	/*
	cout << "a set: " << endl; 
	for(auto w : o_w) {
		cout << w << " ";
	}
	cout << endl;
	*/
	/////////////////////////////////////////////////////////////

	outputs_out.insert(outputs_out.end(), o_w.begin(), o_w.end());
	sort(outputs_out.begin(), outputs_out.end());

	/////////////////////////////////////////////////////////////
	/*
	cout << "a output: " << endl; 
	for(auto w : outputs_out) {
		cout << w << " ";
	}
	cout << endl;
	*/
	/////////////////////////////////////////////////////////////

	assert(o_w.size() == outputs_out.size());
}

//
//=============================================================================
GenTotTree *igentot_new(ClauseSet& dest, vector<int>& lhs, vector<unsigned> & weights, unsigned rhs, int& top)
{
	unsigned n = lhs.size();
	deque<GenTotTree *> nqueue;

	for (unsigned i = 0; i < n; ++i) {
		GenTotTree *tree = new GenTotTree();

		tree->vars.reserve(1);
		tree->w.resize(1);
		tree->vars[weights[i]]   = lhs[i];
		tree->w[0] = weights[i];
		tree->nof_input = 1;
		tree->left      = 0;
		tree->right     = 0;

		nqueue.push_back(tree);
	}

	while (nqueue.size() > 1) {
		GenTotTree *l = nqueue.front();
		nqueue.pop_front();
		GenTotTree *r = nqueue.front();
		nqueue.pop_front();

		GenTotTree *node = new GenTotTree();

		prep_outputs(l->w, r->w, node->w, rhs);

		node->nof_input = node->w.size();
		node->left      = l;
		node->right     = r;

		node->vars.reserve(node->w.size());

		for (unsigned i = 0; i < node->w.size(); ++i) {
			node->vars[node->w[i]] = ++top;
		}

		////////DEBUG
		/*
		cout << "L nodes: ";
		for (auto & it : l->vars) {
			cout << " w: " << it.first << " v: " << it.second << " * ";
		}
		cout << endl;

		cout << "R nodes: ";
		for (auto & it : r->vars) {
			cout << " w: " << it.first << " v: " << it.second << " * ";
		}
		cout << endl;


		cout << "New nodes: ";
		for (auto & it : node->vars) {
			cout << " w: " << it.first << " v: " << it.second << " * ";
		}
		cout << endl;

		cout << " l->w " << l->w.size() << " r->w " << r->w.size() << " a->w " << node->w.size() << " top: " << top << endl;
		cout << " " << top << endl;
		*/
		////////////////

		igentot_new_ua(top, dest, node->vars, node->w, rhs, l->vars, l->w, r->vars, r->w);
		nqueue.push_back(node);
	}

	return nqueue.front();
}

// recursive destruction of the tree
//=============================================================================
static void igentot_destroy(GenTotTree *tree)
{
	if (tree->left )
		igentot_destroy(tree->left );
	if (tree->right)
		igentot_destroy(tree->right);

	tree->vars.clear();
	delete tree;
}

#endif // IGENTOT_HH_
