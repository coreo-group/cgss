/*
 * py.cc
 *
 *  Created on: Feb 11 2020
 *      Author: Jeremias Berg
 *      E-mail: jeremias.berg@helsinki.fi
 */

#define PY_SSIZE_T_CLEAN

#include <setjmp.h>
#include <signal.h>
#include <stdio.h>
#include <iostream>
#include <Python.h>

#include "igentot.hh"

using namespace std;

// docstrings
//=============================================================================
static char    module_docstring[] = "This module provides an interface for "
                                    "encoding a few types of pb "
                                    "constraints";
static char  igentot_new_docstring[] = "Create an iterative gen-totalizer object for "
                                    "an AtMost(k) constraint.";
static char  igentot_inc_docstring[] = "Increase bound in an iterative gen-totalizer "
                                    "object.";
static char  igentot_ext_docstring[] = "Extends the set of inputs in an iterative"
                                    "gen totalizer object.";
static char  igentot_mrg_docstring[] = "Merge two totalizer objects into one.";
static char  igentot_del_docstring[] = "Delete an iterative totalizer object";


static PyObject* PBError;
static jmp_buf env;

// function declaration for functions available in module
//=============================================================================
extern "C" {
	static PyObject *py_igentot_new        (PyObject *, PyObject *);
	static PyObject *py_igentot_del        (PyObject *, PyObject *);
}

// module specification
//=============================================================================
static PyMethodDef module_methods[] = {
	{ "igentot_new",       py_igentot_new,       METH_VARARGS, igentot_new_docstring },
	{ "igentot_del",       py_igentot_del,       METH_VARARGS, igentot_del_docstring },
	{ NULL, NULL, 0, NULL }
};

extern "C" {

// signal handler for SIGINT
//=============================================================================
static void sigint_handler(int signum)
{
	longjmp(env, -1);
}

#if PY_MAJOR_VERSION >= 3  // for Python3
// PyInt_asLong()
//=============================================================================
static int pyint_to_cint(PyObject *i_obj)
{
	return PyLong_AsLong(i_obj);
}

// PyInt_fromLong()
//=============================================================================
static PyObject *pyint_from_cint(int i)
{
	return PyLong_FromLong(i);
}

// PyCapsule_New()
//=============================================================================
static PyObject *void_to_pyobj(void *ptr)
{
	return PyCapsule_New(ptr, NULL, NULL);
}

// PyCapsule_GetPointer()
//=============================================================================
static void *pyobj_to_void(PyObject *obj)
{
	return PyCapsule_GetPointer(obj, NULL);
}

static const char* pyobj_to_string(PyObject *obj) {
    return PyUnicode_AsUTF8(obj);
}

// PyInt_Check()
//=============================================================================
static int pyint_check(PyObject *i_obj)
{
	return PyLong_Check(i_obj);
}

// module initialization
//=============================================================================
static struct PyModuleDef module_def = {
	PyModuleDef_HEAD_INIT,
	"pypb",          /* m_name */
	module_docstring,  /* m_doc */
	-1,                /* m_size */
	module_methods,    /* m_methods */
	NULL,              /* m_reload */
	NULL,              /* m_traverse */
	NULL,              /* m_clear */
	NULL,              /* m_free */
};

PyMODINIT_FUNC PyInit_pypb(void)
{
	PyObject *m = PyModule_Create(&module_def);

	if (m == NULL)
		return NULL;

	PBError = PyErr_NewException((char *)"pypb.error", NULL, NULL);
	Py_INCREF(PBError);

	if (PyModule_AddObject(m, "error", PBError) < 0) {
		Py_DECREF(PBError);
		return NULL;
	}

	return m;
}
#else  // for Python2
// PyInt_asLong()
//=============================================================================
static int pyint_to_cint(PyObject *i_obj)
{
	return PyInt_AsLong(i_obj);
}

// PyInt_fromLong()
//=============================================================================
static PyObject *pyint_from_cint(int i)
{
	return PyInt_FromLong(i);
}

// PyCObject_FromVoidPtr()
//=============================================================================
static PyObject *void_to_pyobj(void *ptr)
{
	return PyCObject_FromVoidPtr(ptr, NULL);
}

// PyCObject_AsVoidPtr()
//=============================================================================
static void *pyobj_to_void(PyObject *obj)
{
	return PyCObject_AsVoidPtr(obj);
}

// PyInt_Check()
//=============================================================================
static int pyint_check(PyObject *i_obj)
{
	return PyInt_Check(i_obj);
}

static const char* pyobj_to_string(PyObject *obj) {
    return PyString_AsString(obj);
}

// module initialization
//=============================================================================
PyMODINIT_FUNC initpypb(void)
{
	PyObject *m = Py_InitModule3("pypb", module_methods,
			module_docstring);

	if (m == NULL)
		return;

	PBError = PyErr_NewException((char *)"pypb.error", NULL, NULL);
	Py_INCREF(PBError);
	PyModule_AddObject(m, "error", PBError);
}
#endif

// auxiliary function for translating an iterable to a vector<int>
//=============================================================================
static bool pyiter_to_vector(PyObject *obj, vector<int>& vect)
{
	PyObject *i_obj = PyObject_GetIter(obj);

	if (i_obj == NULL) {
		PyErr_SetString(PyExc_RuntimeError,
				"Object does not seem to be an iterable.");
		return false;
	}

	PyObject *l_obj;
	while ((l_obj = PyIter_Next(i_obj)) != NULL) {
		if (!pyint_check(l_obj)) {
			Py_DECREF(l_obj);
			Py_DECREF(i_obj);
			PyErr_SetString(PyExc_TypeError, "integer expected");
			return false;
		}

		int l = pyint_to_cint(l_obj);
		Py_DECREF(l_obj);

		if (l == 0) {
			Py_DECREF(i_obj);
			PyErr_SetString(PyExc_ValueError, "non-zero integer expected");
			return false;
		}

		vect.push_back(l);
	}

	Py_DECREF(i_obj);
	return true;
}

static bool pyiter_to_w_vector(PyObject *obj, vector<unsigned>& vect)
{
	PyObject *i_obj = PyObject_GetIter(obj);

	if (i_obj == NULL) {
		PyErr_SetString(PyExc_RuntimeError,
				"Object does not seem to be an iterable.");
		return false;
	}

	PyObject *l_obj;
	while ((l_obj = PyIter_Next(i_obj)) != NULL) {
		if (!pyint_check(l_obj)) {
			Py_DECREF(l_obj);
			Py_DECREF(i_obj);
			PyErr_SetString(PyExc_TypeError, "integer expected");
			return false;
		}

		unsigned l = pyint_to_cint(l_obj);
		Py_DECREF(l_obj);

		if (l == 0) {
			Py_DECREF(i_obj);
			PyErr_SetString(PyExc_ValueError, "non-zero integer expected");
			return false;
		}

		vect.push_back(l);
	}

	Py_DECREF(i_obj);
	return true;
}

static bool pyiter_to_vectorvector(PyObject* obj, vector<vector<int> >& vect) {
	PyObject *i_obj = PyObject_GetIter(obj);

	if (i_obj == NULL) {
		PyErr_SetString(PyExc_RuntimeError,
				"Object does not seem to be an iterable.");
		return false;
	}

	PyObject *l_obj;
	while ((l_obj = PyIter_Next(i_obj)) != NULL) {
        vect.emplace_back();
        if (!pyiter_to_vector(l_obj, vect.back())) {
			Py_DECREF(l_obj);
			Py_DECREF(i_obj);
			return false;
		}
	}
	Py_DECREF(i_obj);
	return true;
}

//
//=============================================================================
static PyObject *py_igentot_new(PyObject *self, PyObject *args)
{
	PyObject *lhs_obj;
	PyObject *w_obj;
	int rhs;
	int top;
	int main_thread;


	if (!PyArg_ParseTuple(args, "OOiii", &lhs_obj, &w_obj,  &rhs, &top, &main_thread))
		return NULL;

	vector<int> lhs;
	if (pyiter_to_vector(lhs_obj, lhs) == false)
		return NULL;
	vector<unsigned> weights; 
	if (pyiter_to_w_vector(w_obj, weights) == false)
		return NULL;


	PyOS_sighandler_t sig_save;
	if (main_thread) {
		sig_save = PyOS_setsig(SIGINT, sigint_handler);

		if (setjmp(env) != 0) {
			PyErr_SetString(PBError, "Caught keyboard interrupt");
			return NULL;
		}
	}

	// calling encoder
	ClauseSet dest;
	GenTotTree *tree = igentot_new(dest, lhs, weights, rhs, top);

	if (main_thread)
		PyOS_setsig(SIGINT, sig_save);

	// creating the resulting clause set
	PyObject *dest_obj = PyList_New(dest.size());
	for (size_t i = 0; i < dest.size(); ++i) {
		PyObject *cl_obj = PyList_New(dest[i].size());

		for (size_t j = 0; j < dest[i].size(); ++j) {
			PyObject *lit_obj = pyint_from_cint(dest[i][j]);
			PyList_SetItem(cl_obj, j, lit_obj);
		}

		PyList_SetItem(dest_obj, i, cl_obj);
	}

	// creating the upper-bounds (right-hand side)
	PyObject *ubs_obj = PyDict_New();
	for (auto& it: tree->vars) {
		PyObject *w  = pyint_from_cint(it.first);
		PyObject *var = pyint_from_cint(it.second);
		PyDict_SetItem(ubs_obj, w, var);
	}

	PyObject *ret = Py_BuildValue("OOOn", void_to_pyobj((void *)tree),
				dest_obj, ubs_obj, (Py_ssize_t)top);

	Py_DECREF(dest_obj);
	Py_DECREF(ubs_obj);
	return ret;
}

//
//=============================================================================
static PyObject *py_igentot_del(PyObject *self, PyObject *args)
{
	PyObject *t_obj;

	if (!PyArg_ParseTuple(args, "O", &t_obj))
		return NULL;

	// get pointer to tree
	GenTotTree *tree = (GenTotTree *)pyobj_to_void(t_obj);

	// delete
	igentot_destroy(tree);

	PyObject *ret = Py_BuildValue("");
	return ret;
}
}  // extern "C"
