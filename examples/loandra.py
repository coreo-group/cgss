#!/usr/bin/env python
#-*- coding:utf-8 -*-
##
## loandra.py
##
##  Created on: Feb 11, 2021
##      Author: Jeremias Berg
##      E-mail: jeremias.berg@helsinki.fi
##

"""
    ===============
    List of classes
    ===============

    .. autosummary::
        :nosignatures:

        RC2
        RC2Stratified

    ==================
    Module description
    ==================

    An implementation of the RC2 algorithm for solving maximum
    satisfiability. RC2 stands for *relaxable cardinality constraints*
    (alternatively, *soft cardinality constraints*) and represents an
    improved version of the OLLITI algorithm, which was described in
    [1]_ and [2]_ and originally implemented in the `MSCG MaxSAT
    solver <https://reason.di.fc.ul.pt/wiki/doku.php?id=mscg>`_.

    Initially, this solver was supposed to serve as an example of a possible
    PySAT usage illustrating how a state-of-the-art MaxSAT algorithm could be
    implemented in Python and still be efficient. It participated in the
    `MaxSAT Evaluations 2018
    <https://maxsat-evaluations.github.io/2018/rankings.html>`_ and `2019
    <https://maxsat-evaluations.github.io/2019/rankings.html>`_ where,
    surprisingly, it was ranked first in two complete categories: *unweighted*
    and *weighted*. A brief solver description can be found in [3]_. A more
    detailed solver description can be found in [4]_.

    .. [1] António Morgado, Carmine Dodaro, Joao Marques-Silva.
        *Core-Guided MaxSAT with Soft Cardinality Constraints*. CP
        2014. pp. 564-573

    .. [2] António Morgado, Alexey Ignatiev, Joao Marques-Silva.
        *MSCG: Robust Core-Guided MaxSAT Solving*. JSAT 9. 2014.
        pp. 129-134

    .. [3] Alexey Ignatiev, António Morgado, Joao Marques-Silva.
        *RC2: A Python-based MaxSAT Solver*. MaxSAT Evaluation 2018.
        p. 22

    .. [4] Alexey Ignatiev, António Morgado, Joao Marques-Silva.
        *RC2: An Efficient MaxSAT Solver*. MaxSAT Evaluation 2018.
        JSAT 11. 2019. pp. 53-64

    The file implements two classes: :class:`RC2` and
    :class:`RC2Stratified`. The former class is the basic
    implementation of the algorithm, which can be applied to a MaxSAT
    formula in the :class:`.WCNF` format. The latter class
    additionally implements Boolean lexicographic optimization (BLO)
    [5]_ and stratification [6]_ on top of :class:`RC2`.

    .. [5] Joao Marques-Silva, Josep Argelich, Ana Graça, Inês Lynce.
        *Boolean lexicographic optimization: algorithms &
        applications*. Ann. Math. Artif. Intell. 62(3-4). 2011.
        pp. 317-343

    .. [6] Carlos Ansótegui, Maria Luisa Bonet, Joel Gabàs, Jordi
        Levy. *Improving WPM2 for (Weighted) Partial MaxSAT*. CP
        2013. pp. 117-132

    The implementation can be used as an executable (the list of
    available command-line options can be shown using ``rc2.py -h``)
    in the following way:

    ::

        $ xzcat formula.wcnf.xz
        p wcnf 3 6 4
        1 1 0
        1 2 0
        1 3 0
        4 -1 -2 0
        4 -1 -3 0
        4 -2 -3 0

        $ rc2.py -vv formula.wcnf.xz
        c formula: 3 vars, 3 hard, 3 soft
        c cost: 1; core sz: 2; soft sz: 2
        c cost: 2; core sz: 2; soft sz: 1
        s OPTIMUM FOUND
        o 2
        v -1 -2 3
        c oracle time: 0.0001

    Alternatively, the algorithm can be accessed and invoked through the
    standard ``import`` interface of Python, e.g.

    .. code-block:: python

        >>> from pysat.examples.rc2 import RC2
        >>> from pysat.formula import WCNF
        >>>
        >>> wcnf = WCNF(from_file='formula.wcnf.xz')
        >>>
        >>> with RC2(wcnf) as rc2:
        ...     for m in rc2.enumerate():
        ...         print('model {0} has cost {1}'.format(m, rc2.cost))
        model [-1, -2, 3] has cost 2
        model [1, -2, -3] has cost 2
        model [-1, 2, -3] has cost 2
        model [-1, -2, -3] has cost 3

    As can be seen in the example above, the solver can be instructed
    either to compute one MaxSAT solution of an input formula, or to
    enumerate a given number (or *all*) of its top MaxSAT solutions.

    ==============
    Module details
    ==============
"""

#
#==============================================================================
from __future__ import print_function
import collections
import getopt
import itertools
from math import copysign
import os
from pysat.formula import CNFPlus, WCNFPlus
from pysat.card import ITotalizer, ITotalizerLazier, SSEncoder
from pysat.pb import IGENTotalizer
import pysat.exch
from pysat.solvers import Solver, SolverNames
import re
import six
from six.moves import range
import sys
import count
import random
import time
import gc
#gc.set_debug(gc.DEBUG_STATS)
from pysat.maxpre import preprocess_formula

#
#==============================================================================
class RC2WCE(object):
    """
        Implementation of the basic RC2 algorithm. Given a (weighted)
        (partial) CNF formula, i.e. formula in the :class:`.WCNF`
        format, this class can be used to compute a given number of
        MaxSAT solutions for the input formula. :class:`RC2` roughly
        follows the implementation of algorithm OLLITI [1]_ [2]_ of
        MSCG and applies a few heuristics on top of it. These include

        - *unsatisfiable core exhaustion* (see method :func:`exhaust_core`),
        - *unsatisfiable core reduction* (see method :func:`minimize_core`),
        - *intrinsic AtMost1 constraints* (see method :func:`adapt_am1`).

        :class:`RC2` can use any SAT solver available in PySAT. The
        default SAT solver to use is ``g3`` (see
        :class:`.SolverNames`). Additionally, if Glucose is chosen,
        the ``incr`` parameter controls whether to use the incremental
        mode of Glucose [7]_ (turned off by default). Boolean
        parameters ``adapt``, ``exhaust``, and ``minz`` control
        whether or to apply detection and adaptation of intrinsic
        AtMost1 constraints, core exhaustion, and core reduction.
        Unsatisfiable cores can be trimmed if the ``trim`` parameter
        is set to a non-zero integer. Finally, verbosity level can be
        set using the ``verbose`` parameter.

        .. [7] Gilles Audemard, Jean-Marie Lagniez, Laurent Simon.
            *Improving Glucose for Incremental SAT Solving with
            Assumptions: Application to MUS Extraction*. SAT 2013.
            pp. 309-317

        :param formula: (weighted) (partial) CNF formula
        :param solver: SAT oracle name
        :param adapt: detect and adapt intrinsic AtMost1 constraints
        :param exhaust: do core exhaustion
        :param incr: use incremental mode of Glucose
        :param minz: do heuristic core reduction
        :param trim: do core trimming at most this number of times
        :param verbose: verbosity level

        :type formula: :class:`.WCNF`
        :type solver: str
        :type adapt: bool
        :type exhaust: bool
        :type incr: bool
        :type minz: bool
        :type trim: int
        :type verbose: int
    """

    def __init__(self, formula, solver='g3', adapt=False, fix_dec_order=0, group_softs=0, exhaust=False, incr=False, minz=False,
            structure_sharing_opts=(1,16), trim=0, verbose=0, add_cores=0, disable_learning=False, pbc_opts=3, use_lower_bounds=False, use_upper_bounds=False,
            lazy_left=False, sdivp=2.0, strat_lower_bounds=False, speed_up_assumps=0, shuffle_core=False, deactivate=True, pmres=False):
        """
            Constructor.
        """
        # saving verbosity level and other options
        self.verbose = verbose
        self.exhaust = exhaust
        self.solver = solver
        self.adapt = adapt
        self.fix_dec_order=fix_dec_order
        self.group_softs = group_softs
        self.minz = minz

        if not structure_sharing_opts: #TODO: better parameter
            self.ss_separate_relax=True
            self.ss_options=(0,1000000)
            self.use_ss=True
        else:
            self.ss_separate_relax=False
            self.ss_options=structure_sharing_opts
            self.use_ss=True

        self.trim = trim
        self.add_cores = add_cores
        self.disable_learning = disable_learning
        self.pbc_opts = pbc_opts
        self.use_lower_bounds = use_lower_bounds
        self.use_upper_bounds = use_upper_bounds
        self.lazy_left = lazy_left
        self.sdivp = sdivp
        self.strat_lower_bounds = strat_lower_bounds
        self.speed_up_assumps_strategy=speed_up_assumps
        self.shuffle_core=shuffle_core
        self.deactivate_lits=deactivate
        self.pmres=pmres
        if self.use_ss: self.ssenc = SSEncoder(self.ss_options[0], self.ss_options[1], None, pmres)

        # clause selectors and mapping from selectors to clause ids
        self.sels, self.smap, self.sall = [], {}, []

        # other MaxSAT related stuff
        self.topv = formula.nv
        self.wght = {}  # weights of soft clauses
        self.sums = []  # totalizer sum assumptions
        self.bnds = {}  # a mapping from sum assumptions to totalizer bounds
        self.tobj = {}  # a mapping from sum assumptions to totalizer objects
        self.cost = 0
        self.lower_bound_PBC_ref=0


        #lazy left stuff, keep list of totalizer objects to activate when cost goes to zero
        self.tot_lazy_inputs={}
    
        self.sels_to_deactivate=set()
        self.sums_to_deactivate=set()

        #stats
        self.stats_iters=0
        self.stats_cores=0
        self.stats_coresize_sum=0
        self.stats_coresizes=[]
        self.stats_wce_coresizes=[]
        self.stats_assumpssize_sum=0
        self.stats_assumpssizes=[]
        self.stats_wce_assumpssizes=[]
        self.stats_wce_cores=[]
        self.stats_sums=[]
        self.stats_sum_variables={}
        self._time_wcores=0.0
        self._time_satsolver=0.0
        self.wcores=[]

        # mappings between internal and external variables
        VariableMap = collections.namedtuple('VariableMap', ['e2i', 'i2e'])
        self.vmap = VariableMap(e2i={}, i2e={})

        # initialize SAT oracle with hard clauses only
        self.init(formula, incr=incr)

        # core minimization is going to be extremely expensive
        # for large plain formulas, and so we turn it off here
        wght = self.wght.values()
        if not formula.hard and len(self.sels) > 100000 and min(wght) == max(wght):
            self.minz = False

    def __del__(self):
        """
            Destructor.
        """
        self.delete()

    def __enter__(self):
        """
            'with' constructor.
        """
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        """
            'with' destructor.
        """
        self.delete()

    def init(self, formula, incr=False):
        """
            Initialize the internal SAT oracle. The oracle is used
            incrementally and so it is initialized only once when
            constructing an object of class :class:`RC2`. Given an
            input :class:`.WCNF` formula, the method bootstraps the
            oracle with its hard clauses. It also augments the soft
            clauses with "fresh" selectors and adds them to the oracle
            afterwards.

            Optional input parameter ``incr`` (``False`` by default)
            regulates whether or not Glucose's incremental mode [7]_
            is turned on.

            :param formula: input formula
            :param incr: apply incremental mode of Glucose

            :type formula: :class:`.WCNF`
            :type incr: bool
        """

        # creating a solver object
        self.oracle = Solver(name=self.solver, bootstrap_with=formula.hard,
                incr=incr, use_timer=True)

        if self.solver in SolverNames.glucose3:
            self.oracle.fix_decision_order(self.fix_dec_order)
            self.oracle.speed_up_assumps(self.speed_up_assumps_strategy)

        if self.solver in SolverNames.minisatpbc:
            self.oracle.solver.set_pbc_opts(self.pbc_opts)

        # adding native cardinality constraints (if any) as hard clauses
        # this can be done only if the Minicard solver is in use
        # this cannot be done if RC2 is run from the command line
        if isinstance(formula, WCNFPlus) and formula.atms:
            assert self.solver in SolverNames.minicard, \
                    'Only Minicard supports native cardinality constraints. Make sure you use the right type of formula.'

            for atm in formula.atms:
                self.oracle.add_atmost(*atm)

        self.sels_orig=[]
        self.sels_in_core_s=set()
        self.sels_in_core=[]
        self.weights_orig={}

        self.UB=1 # upper bound for cost

        # adding soft clauses to oracle
        for i, cl in enumerate(formula.soft):
            selv = cl[0]  # if clause is unit, selector variable is its literal

            if len(cl) > 1:
                self.topv += 1
                selv = self.topv

                cl.append(-self.topv)
                self.oracle.add_clause(cl)

            self.UB+=formula.wght[i]
            if selv not in self.wght:
                # record selector and its weight
                self.sels.append(selv)
                self.wght[selv] = formula.wght[i]
                self.smap[selv] = i

                self.sels_orig.append(selv)
                self.weights_orig[selv] = formula.wght[i]
            else:
                # selector is not new; increment its weight
                self.wght[selv] += formula.wght[i]
                self.weights_orig[selv]+=formula.wght[i]

        # storing the set of selectors
        self.sels_set = set(self.sels)
        self.sall = self.sels[:]

        # at this point internal and external variables are the same
        for v in range(1, formula.nv + 1):
            self.vmap.e2i[v] = v
            self.vmap.i2e[v] = v

        if self.verbose > 1:
            print('c formula: {0} vars, {1} hard, {2} soft'.format(formula.nv,
                len(formula.hard), len(formula.soft)))

        self.initial_formula=formula

        if self.group_softs:
            groups=pysat.exch.get_exchangeables(self.initial_formula.hard+self.initial_formula.soft, [i[0] for i in self.initial_formula.soft], abs(self.group_softs))
            self.soft_groups={}
            if self.group_softs>0:
                for gg in groups:
                    g=[l for l in gg if not l in self.soft_groups]
                    gset=set(g)
                    if len(g)!=len(gset):g=list(gset)
                    if len(g)<2: continue
                    for l in g:
                        if l in self.soft_groups: continue
                        self.soft_groups[l]=g

    def add_clause(self, clause, weight=None):
        """
            The method for adding a new hard of soft clause to the
            problem formula. Although the input formula is to be
            specified as an argument of the constructor of
            :class:`RC2`, adding clauses may be helpful when
            *enumerating* MaxSAT solutions of the formula. This way,
            the clauses are added incrementally, i.e. *on the fly*.

            The clause to add can be any iterable over integer
            literals. The additional integer parameter ``weight`` can
            be set to meaning the the clause being added is soft
            having the corresponding weight (note that parameter
            ``weight`` is set to ``None`` by default meaning that the
            clause is hard).

            :param clause: a clause to add
            :param weight: weight of the clause (if any)

            :type clause: iterable(int)
            :type weight: int

            .. code-block:: python

                >>> from pysat.examples.rc2 import RC2
                >>> from pysat.formula import WCNF
                >>>
                >>> wcnf = WCNF()
                >>> wcnf.append([-1, -2])  # adding hard clauses
                >>> wcnf.append([-1, -3])
                >>>
                >>> wcnf.append([1], weight=1)  # adding soft clauses
                >>> wcnf.append([2], weight=1)
                >>> wcnf.append([3], weight=1)
                >>>
                >>> with RC2(wcnf) as rc2:
                ...     rc2.compute()  # solving the MaxSAT problem
                [-1, 2, 3]
                ...     print(rc2.cost)
                1
                ...     rc2.add_clause([-2, -3])  # adding one more hard clause
                ...     rc2.compute()  # computing another model
                [-1, -2, 3]
                ...     print(rc2.cost)
                2
        """
        # first, map external literals to internal literals
        # introduce new variables if necessary
        cl = list(map(lambda l: self._map_extlit(l), clause if not len(clause) == 2 or not type(clause[0]) == list else clause[0]))

        if not weight:
            if not len(clause) == 2 or not type(clause[0]) == list:
                # the clause is hard, and so we simply add it to the SAT oracle
                self.oracle.add_clause(cl)
            else:
                # this should be a native cardinality constraint,
                # which can be used only together with Minicard
                assert self.solver in SolverNames.minicard, \
                        'Only Minicard supports native cardinality constraints.'

                self.oracle.add_atmost(cl, clause[1])
        else:
            # soft clauses should be augmented with a selector
            selv = cl[0]  # for a unit clause, no selector is needed

            if len(cl) > 1:
                self.topv += 1
                selv = self.topv

                cl.append(-self.topv)
                self.oracle.add_clause(cl)

            if selv not in self.wght:
                # record selector and its weight
                self.sels.append(selv)
                self.wght[selv] = weight
                self.smap[selv] = len(self.sels) - 1
            else:
                # selector is not new; increment its weight
                self.wght[selv] += weight

            self.sall.append(selv)
            self.sels_set.add(selv)

    def delete(self):
        """
            Explicit destructor of the internal SAT oracle and all the
            totalizer objects creating during the solving process.
        """

        if self.oracle:
            self.oracle.delete()
            self.oracle = None

            if self.solver not in SolverNames.minicard:  # for minicard, there is nothing to free
                for t in six.itervalues(self.tobj):
                    t.delete()

    def compute(self):
        """
            This method can be used for computing one MaxSAT solution,
            i.e. for computing an assignment satisfying all hard
            clauses of the input formula and maximizing the sum of
            weights of satisfied soft clauses. It is a wrapper for the
            internal :func:`compute_` method, which does the job,
            followed by the model extraction.

            Note that the method returns ``None`` if no MaxSAT model
            exists. The method can be called multiple times, each
            being followed by blocking the last model. This way one
            can enumerate top-:math:`k` MaxSAT solutions (this can
            also be done by calling :meth:`enumerate()`).

            :returns: a MaxSAT model
            :rtype: list(int)

            .. code-block:: python

                >>> from pysat.examples.rc2 import RC2
                >>> from pysat.formula import WCNF
                >>>
                >>> rc2 = RC2(WCNF())  # passing an empty WCNF() formula
                >>> rc2.add_clause([-1, -2])
                >>> rc2.add_clause([-1, -3])
                >>> rc2.add_clause([-2, -3])
                >>>
                >>> rc2.add_clause([1], weight=1)
                >>> rc2.add_clause([2], weight=1)
                >>> rc2.add_clause([3], weight=1)
                >>>
                >>> model = rc2.compute()
                >>> print(model)
                [-1, -2, 3]
                >>> print(rc2.cost)
                2
                >>> rc2.delete()
        """
        # simply apply MaxSAT only once
        res = self.compute_()

        if res:
            # extracting a model
            if self.cost==self.UB:
                self.model=self.best_model
            else:
                self.model = self.oracle.get_model()

            if not self.model: # UB==self.cost may cause that there is no model
                #TODO: what if model is actually wanted..?
                return None

            self.model = filter(lambda l: abs(l) in self.vmap.i2e, self.model)
            self.model = map(lambda l: int(copysign(self.vmap.i2e[abs(l)], l)), self.model)
            self.model = sorted(self.model, key=lambda l: abs(l))

            return self.model

    def enumerate(self, block_mcses=False):
        """
            Enumerate top MaxSAT solutions (from best to worst). The
            method works as a generator, which iteratively calls
            :meth:`compute` to compute a MaxSAT model, blocks it
            internally and returns it.

            :returns: a MaxSAT model
            :rtype: list(int)

            .. code-block:: python

                >>> from pysat.examples.rc2 import RC2
                >>> from pysat.formula import WCNF
                >>>
                >>> rc2 = RC2(WCNF())  # passing an empty WCNF() formula
                >>> rc2.add_clause([-1, -2])  # adding clauses "on the fly"
                >>> rc2.add_clause([-1, -3])
                >>> rc2.add_clause([-2, -3])
                >>>
                >>> rc2.add_clause([1], weight=1)
                >>> rc2.add_clause([2], weight=1)
                >>> rc2.add_clause([3], weight=1)
                >>>
                >>> for model in rc2.enumerate():
                ...     print(model, rc2.cost)
                [-1, -2, 3] 2
                [1, -2, -3] 2
                [-1, 2, -3] 2
                [-1, -2, -3] 3
                >>> rc2.delete()
        """
        done = False
        while not done:
            model = self.compute()

            if model != None:
                if block_mcses:
                    # a little bit low-level stuff goes here
                    # to block an MCS corresponding to the model
                    m = set(self.oracle.get_model())
                    self.oracle.add_clause([-l for l in filter(lambda l: l in m, self.sall)])
                else:
                    self.add_clause([-l for l in model])

                yield model
            else:
                done = True

    def compute_(self):
        """
            Main core-guided loop, which iteratively calls a SAT
            oracle, extracts a new unsatisfiable core and processes
            it. The loop finishes as soon as a satisfiable formula is
            obtained. If specified in the command line, the method
            additionally calls :meth:`adapt_am1` to detect and adapt
            intrinsic AtMost1 constraints before executing the loop.

            :rtype: bool
        """
        # trying to adapt (simplify) the formula
        # by detecting and using atmost1 constraints
        if self.adapt:
            self.adapt_am1()


        if self.use_upper_bounds and self.cost==self.UB:
            print("c skip solving, self.cost==self.UB")
            return True

        if self.disable_learning: self.oracle.solve()
        # main solving loop
        while 1:
            self.stats_wce_coresizes.append([])
            self.stats_wce_assumpssizes.append([])
            a=time.time()
            while not self.oracle.solve(assumptions=self.sels + self.sums):
                self._time_satsolver+=time.time()-a
                if self.disable_learning:
                    self.oracle.disable_learning()

                self.stats_iters+=1
                self.stats_assumpssizes.append(len(self.sels)+len(self.sums))
                self.stats_wce_assumpssizes[-1].append(len(self.sels)+len(self.sums))
                self.stats_assumpssize_sum+=len(self.sels)+len(self.sums)
                
                self.get_core()
                if not self.core:
                    # core is empty, i.e. hard part is unsatisfiable
                    return False

                self.stats_cores+=1
                self.stats_coresizes.append(len(self.core))
                self.stats_wce_coresizes[-1].append(len(self.core))
                self.stats_coresize_sum+=len(self.core)

                if len(self.core) < self.add_cores: self.oracle.add_clause([-l for l in self.core])
                elif len(self.core) < -self.add_cores: self.oracle.add_clause_as_learnt([-l for l in self.core])

                self.process_core()

                if self.use_upper_bounds and self.cost==self.UB:
                    print("c no more cores needed, self.cost==self.UB")
                    return True
                    

                self.handle_lower_bound()

                if self.verbose > 1:
                    print('c cost: {0}; core sz: {1}; soft sz: {2}'.format(self.cost,
                        len(self.core), len(self.sels) + len(self.sums)))
                a=time.time()
            self._time_satsolver+=time.time()-a

            # handle upper bound
            if self.use_upper_bounds:
                model = set(self.oracle.get_model())
                mcost=0
                for sel in self.sels_orig:
                    if sel not in model: mcost+=self.weights_orig[sel]
                if mcost<self.UB:
                    self.best_model=model
                    self.UB=mcost
                    if self.cost==self.UB:
                        print("c final model found, self.cost==self.UB")
                        return True
                    if self.cost>self.UB:
                        print("c FAILLL")

            self.stats_wce_cores.append(len(self.wcores))
            if not self.process_wcores(): break
        
        self.stats_iters+=1
        self.stats_coresizes.append(0) 
        self.stats_assumpssizes.append(len(self.sels)+len(self.sums))
        self.stats_assumpssize_sum+=len(self.sels)+len(self.sums)
        return True

    def in_core(self, lit):
        if lit not in self.sels_in_core_s:
            self.sels_in_core_s.add(lit)
            self.sels_in_core.append(lit)

    def handle_lower_bound(self):
        if not self.use_lower_bounds: return
        if self.solver not in SolverNames.minisatpbc:  return
        if not self.strat_lower_bounds:
            
            self.weight_sum = sum(self.weights_orig.values())
            wghts=[self.weights_orig[i] for i in self.sels_orig]
                
            ok, self.lower_bound_PBC_ref = self.oracle.add_pbc([i for i in self.sels_orig], wghts, self.weight_sum - self.cost)
            return
            if self.lower_bound_PBC_ref:
                self.oracle.update_pbc(self.lower_bound_PBC_ref, self.weight_sum - self.cost)
            else:
                self.weight_sum = sum(self.weights_orig.values())
                wghts=[self.weights_orig[i] for i in self.sels_orig]
                
                ok, self.lower_bound_PBC_ref = self.oracle.add_pbc([i for i in self.sels_orig], wghts, self.weight_sum - self.cost)
        else:
            if self.lower_bound_PBC_ref:
                if self.sels_in_core_last_size == len(self.sels_in_core):
                    self.oracle.update_pbc(self.lower_bound_PBC_ref, self.weight_sum - self.cost)
                else:
                    self.weight_sum      = sum(self.weights_orig[l] for l in self.sels_in_core)

                    new_lits=[i for i in self.sels_in_core[self.sels_in_core_last_size:]]
                    new_weights=[self.weights_orig[i] for i in new_lits]

                    self.oracle.edit_pbc(self.lower_bound_PBC_ref, new_lits, new_weights, self.weight_sum - self.cost)
            else:
                self.weight_sum = sum([self.weights_orig[i] for i in self.sels_in_core])

                lits =[i for i in self.sels_in_core]
                wghts=[self.weights_orig[i] for i in lits]
                
                ok, self.lower_bound_PBC_ref = self.oracle.add_pbc(lits, wghts, self.weight_sum - self.cost)

            self.sels_in_core_last_size=len(self.sels_in_core)
            
    def coreshuffle(self):
        random.shuffle(self.core)
        """
        core2=[0]*len(self.core)
        j=len(self.core)-1
        for i in range(len(self.core)-1, -1, -1):
            while core2[j]!=0: j-=1
            if j<0: j+=len(self.core)
            core2[j]=self.core[i]
            count=0
            while j>=0 and count<2:
                if core2[j]==0: count+=1
                j-=1
            if j<0: j=len(self.core)-1
        self.core, core2=core2, self.core
        """

    def get_core(self):
        """
            Extract unsatisfiable core. The result of the procedure is
            stored in variable ``self.core``. If necessary, core
            trimming and also heuristic core reduction is applied
            depending on the command-line options. A *minimum weight*
            of the core is computed and stored in ``self.minw``.
            Finally, the core is divided into two parts:

            1. clause selectors (``self.core_sels``),
            2. sum assumptions (``self.core_sums``).
        """

        # extracting the core
        self.core = self.oracle.get_core()
        if self.shuffle_core: self.coreshuffle()

        if self.core:
            # try to reduce the core by trimming
            self.trim_core()

            # and by heuristic minimization
            self.minimize_core()

            # the core may be empty after core minimization
            if not self.core:
                return

            # core weight
            self.minw = min(map(lambda l: self.wght[l], self.core))

            # dividing the core into two parts
            iter1, iter2 = itertools.tee(self.core)
            self.core_sels = list(l for l in iter1 if l in self.sels_set)
            self.core_sums = list(l for l in iter2 if l not in self.sels_set)

    def ssenc_relax(self, cores):
        self.rcores=[i[0] for i in cores]
        new_outputs=self.ssenc.relax(self.rcores, top_id=self.topv)
        if self.ssenc.nof_new:
            for cl in self.ssenc.cnf.clauses[-self.ssenc.nof_new:]:
                self.oracle.add_clause(cl)
        self.topv=self.ssenc.top_id

        for i in range(len(cores)):
            if self.pmres:
                for nlit in new_outputs[i]:
                    lit=-nlit
                    if lit not in self.wght or not self.wght[lit]:
                        self.wght[lit] = cores[i][3]
                        self.sums.append(lit)
                    else:
                        self.wght[lit] += cores[i][3]
                    
            else:
                b, lit = self.exhaust_ssenc_core(-new_outputs[i], cores[i][3]) if self.exhaust else (1, -new_outputs[i])
                if b:
                    if lit not in self.wght or not self.wght[lit]:
                        self.bnds[lit] = b
                        self.wght[lit] = cores[i][3]
                        self.sums.append(lit)
                    else:
                        self.wght[lit] += cores[i][3]
                else:
                    for relv in cores[i][0]:
                        self.oracle.add_clause([relv])
        

    def process_wcores(self):
        if len(self.wcores)==0: return 0
        a=time.time()
        if self.use_ss:
            if self.ss_separate_relax:
                for i in self.wcores: self.ssenc_relax([i])
            else:
                self.ssenc_relax(self.wcores)
            
        else:
            for i in self.wcores:
                self.rels=i[0]
                self.core_sels=i[1]
                self.core_sums=i[2]
                self.minw=i[3]

                # create a new cardinality constraint
                t = self.create_sum()
                self.stats_sums.append(t)

                # apply core exhaustion if required
                b = self.exhaust_core(t) if self.exhaust else 1

                if b:
                    # save the info about this sum and
                    # add its assumption literal
                    self.set_bound(t, b)
                else:
                    # impossible to satisfy any of these clauses
                    # they must become hard
                    for relv in self.rels:
                        self.oracle.add_clause([relv])
        self.wcores=[]
        self._time_wcores+=time.time()-a
        return 1

    def process_core(self):
        """
            The method deals with a core found previously in
            :func:`get_core`. Clause selectors ``self.core_sels`` and
            sum assumptions involved in the core are treated
            separately of each other. This is handled by calling
            methods :func:`process_sels` and :func:`process_sums`,
            respectively. Whenever necessary, both methods relax the
            core literals, which is followed by creating a new
            totalizer object encoding the sum of the new relaxation
            variables. The totalizer object can be "exhausted"
            depending on the option.
        """

        # assumptions to remove
        self.garbage = set()

        # updating the cost
        self.process_sels()
        self.cost += self.minw


        if len(self.core_sels) != 1 or len(self.core_sums) > 0:
            # process selectors in the core
            self.process_sums()
            if len(self.rels)>1: self.wcores.append((self.rels, self.core_sels, self.core_sums, self.minw))
        else:
            # unit cores are treated differently
            # (their negation is added to the hard part)
            self.in_core(self.core_sels[0])

            self.oracle.add_clause([-self.core_sels[0]])
            self.garbage.add(self.core_sels[0])

        # remove unnecessary assumptions
        self.filter_assumps()

    def adapt_am1(self):
        """
            Detect and adapt intrinsic AtMost1 constraints. Assume
            there is a subset of soft clauses
            :math:`\\mathcal{S}'\subseteq \\mathcal{S}` s.t.
            :math:`\sum_{c\in\\mathcal{S}'}{c\leq 1}`, i.e. at most
            one of the clauses of :math:`\\mathcal{S}'` can be
            satisfied.

            Each AtMost1 relationship between the soft clauses can be
            detected in the following way. The method traverses all
            soft clauses of the formula one by one, sets one
            respective selector literal to true and checks whether
            some other soft clauses are forced to be false. This is
            checked by testing if selectors for other soft clauses are
            unit-propagated to be false. Note that this method for
            detection of AtMost1 constraints is *incomplete*, because
            in general unit propagation does not suffice to test
            whether or not :math:`\\mathcal{F}\wedge l_i\\models
            \\neg{l_j}`.

            Each intrinsic AtMost1 constraint detected this way is
            handled by calling :func:`process_am1`.
        """

        # literal connections
        conns = collections.defaultdict(lambda: set([]))
        confl = []

        # prepare connections
        for l1 in self.sels:
            st, props = self.oracle.propagate(assumptions=[l1], phase_saving=2)
            if st:
                for l2 in props:
                    if -l2 in self.sels_set:
                        conns[l1].add(-l2)
                        conns[-l2].add(l1)
            else:
                # propagating this literal results in a conflict
                confl.append(l1)

        if confl:  # filtering out unnecessary connections
            ccopy = {}
            confl = set(confl)

            for l in conns:
                if l not in confl:
                    cc = conns[l].difference(confl)
                    if cc:
                        ccopy[l] = cc

            conns = ccopy
            confl = list(confl)

            # processing unit size cores
            for l in confl:
                self.core, self.minw = [l], self.wght[l]
                self.core_sels, self.core_sums = [l], []
                self.process_core()

            if self.verbose > 1:
                print('c unit cores found: {0}; cost: {1}'.format(len(confl),
                    self.cost))

        nof_am1 = 0
        len_am1 = []
        lits = set(conns.keys())
        while lits:
            am1 = [min(lits, key=lambda l: len(conns[l]))]

            for l in sorted(conns[am1[0]], key=lambda l: len(conns[l])):
                if l in lits:
                    for l_added in am1[1:]:
                        if l_added not in conns[l]:
                            break
                    else:
                        am1.append(l)

            # updating remaining lits and connections
            lits.difference_update(set(am1))
            for l in conns:
                conns[l] = conns[l].difference(set(am1))

            if len(am1) > 1:
                # treat the new atmost1 relation
                self.process_am1(am1)
                nof_am1 += 1
                len_am1.append(len(am1))

        # updating the set of selectors
        self.sels_set = set(self.sels)

        if self.verbose > 1 and nof_am1:
            print('c am1s found: {0}; avgsz: {1:.1f}; cost: {2}'.format(nof_am1,
                sum(len_am1) / float(nof_am1), self.cost))

    def process_am1(self, am1):
        """
            Process an AtMost1 relation detected by :func:`adapt_am1`.
            Note that given a set of soft clauses
            :math:`\\mathcal{S}'` at most one of which can be
            satisfied, one can immediately conclude that the formula
            has cost at least :math:`|\\mathcal{S}'|-1` (assuming
            *unweighted* MaxSAT). Furthermore, it is safe to replace
            all clauses of :math:`\\mathcal{S}'` with a single soft
            clause :math:`\sum_{c\in\\mathcal{S}'}{c}`.

            Here, input parameter ``am1`` plays the role of subset
            :math:`\\mathcal{S}'` mentioned above. The procedure bumps
            the MaxSAT cost by ``self.minw * (len(am1) - 1)``.

            All soft clauses involved in ``am1`` are replaced by a
            single soft clause, which is a disjunction of the
            selectors of clauses in ``am1``. The weight of the new
            soft clause is set to ``self.minw``.

            :param am1: a list of selectors connected by an AtMost1 constraint

            :type am1: list(int)
        """

        # computing am1's weight
        self.minw = min(map(lambda l: self.wght[l], am1))

        # pretending am1 to be a core, and the bound is its size - 1
        self.core_sels, b = am1, len(am1) - 1

        # incrementing the cost
        self.cost += b * self.minw

        # assumptions to remove
        self.garbage = set()

        # splitting and relaxing if needed
        self.process_sels()

        # new selector
        self.topv += 1
        selv = self.topv

        self.oracle.add_clause([-l for l in self.rels] + [-selv])

        # integrating the new selector
        self.sels.append(selv)
        self.wght[selv] = self.minw
        self.smap[selv] = len(self.wght) - 1

        # removing unnecessary assumptions
        self.filter_assumps()

    def trim_core(self):
        """
            This method trims a previously extracted unsatisfiable
            core at most a given number of times. If a fixed point is
            reached before that, the method returns.
        """

        for i in range(self.trim):
            # call solver with core assumption only
            # it must return 'unsatisfiable'
            self.oracle.solve(assumptions=self.core)

            # extract a new core
            new_core = self.oracle.get_core()

            if len(new_core) == len(self.core):
                # stop if new core is not better than the previous one
                break

            # otherwise, update core
            self.core = new_core

    def minimize_core(self):
        """
            Reduce a previously extracted core and compute an
            over-approximation of an MUS. This is done using the
            simple deletion-based MUS extraction algorithm.

            The idea is to try to deactivate soft clauses of the
            unsatisfiable core one by one while checking if the
            remaining soft clauses together with the hard part of the
            formula are unsatisfiable. Clauses that are necessary for
            preserving unsatisfiability comprise an MUS of the input
            formula (it is contained in the given unsatisfiable core)
            and are reported as a result of the procedure.

            During this core minimization procedure, all SAT calls are
            dropped after obtaining 1000 conflicts.
        """

        if self.minz and len(self.core) > 1:
            self.core = sorted(self.core, key=lambda l: self.wght[l])
            self.oracle.conf_budget(1000)

            i = 0
            while i < len(self.core):
                to_test = self.core[:i] + self.core[(i + 1):]

                if self.oracle.solve_limited(assumptions=to_test) == False:
                    self.core = to_test
                else:
                    i += 1


    def exhaust_ssenc_core(self, lit, cost):
        if self.pmres: return 1, lit

        b=1
        while 1:
            if self.oracle.solve(assumptions=[lit]):
                return b, lit
            else:
                self.cost += cost
                self.bnds[lit]=b
                tmp, b=self.update_sum(lit)
                lit = -self.ssenc.get_output(-lit, b)
                if not lit: return None, None
                                
                

    def exhaust_core(self, tobj):
        """
            Exhaust core by increasing its bound as much as possible.
            Core exhaustion was originally referred to as *cover
            optimization* in [6]_.

            Given a totalizer object ``tobj`` representing a sum of
            some *relaxation* variables :math:`r\in R` that augment
            soft clauses :math:`\\mathcal{C}_r`, the idea is to
            increase the right-hand side of the sum (which is equal to
            1 by default) as much as possible, reaching a value
            :math:`k` s.t. formula
            :math:`\\mathcal{H}\wedge\\mathcal{C}_r\wedge(\sum_{r\in
            R}{r\leq k})` is still unsatisfiable while increasing it
            further makes the formula satisfiable (here
            :math:`\\mathcal{H}` denotes the hard part of the
            formula).

            The rationale is that calling an oracle incrementally on a
            series of slightly modified formulas focusing only on the
            recently computed unsatisfiable core and disregarding the
            rest of the formula may be practically effective.
        """
        if self.pmres: return 1

        # the first case is simpler
        if self.oracle.solve(assumptions=[-tobj.rhs[1]]):
            return 1
        else:
            self.cost += self.minw

        for i in range(2, len(self.rels)):
            # saving the previous bound
            self.tobj[-tobj.rhs[i - 1]] = tobj
            self.bnds[-tobj.rhs[i - 1]] = i - 1

            # increasing the bound
            self.update_sum(-tobj.rhs[i - 1])

            if self.oracle.solve(assumptions=[-tobj.rhs[i]]):
                # the bound should be equal to i
                return i

            # the cost should increase further
            self.cost += self.minw

        return None

    def process_sels(self):
        """
            Process soft clause selectors participating in a new core.
            The negation :math:`\\neg{s}` of each selector literal
            :math:`s` participating in the unsatisfiable core is added
            to the list of relaxation literals, which will be later
            used to create a new totalizer object in
            :func:`create_sum`.

            If the weight associated with a selector is equal to the
            minimal weight of the core, e.g. ``self.minw``, the
            selector is marked as garbage and will be removed in
            :func:`filter_assumps`. Otherwise, the clause is split as
            described in [1]_.
        """


        handled=set()

        # new relaxation variables
        self.rels = []
        for l in self.core_sels:
            if l in handled: continue
            self.in_core(l)
            if self.wght[l] == self.minw:
                # marking variable as being a part of the core
                # so that next time it is not used as an assump
                self.garbage.add(l)

                self.rels.append(-l)
            else:
                # do not remove this variable from assumps
                # since it has a remaining non-zero weight
                self.wght[l] -= self.minw

                self.rels.append(-l)



    def process_sums(self):
        """
            Process cardinality sums participating in a new core.
            Whenever necessary, some of the sum assumptions are
            removed or split (depending on the value of
            ``self.minw``). Deleted sums are marked as garbage and are
            dealt with in :func:`filter_assumps`.

            In some cases, the process involves updating the
            right-hand sides of the existing cardinality sums (see the
            call to :func:`update_sum`). The overall procedure is
            detailed in [1]_.
        """

        for l in self.core_sums:
            if self.wght[l] == self.minw:
                # marking variable as being a part of the core
                # so that next time it is not used as an assump
                self.garbage.add(l)
            else:
                # do not remove this variable from assumps
                # since it has a remaining non-zero weight
                self.wght[l] -= self.minw
            # put this assumption to relaxation vars
            self.rels.append(-l)
            if self.pmres: continue

            # increase bound for the sum
            t, b = self.update_sum(l)

            # updating bounds and weights
            if t and b < len(t.rhs): # normal totalizer
                lnew = -t.rhs[b]
                if lnew in self.garbage:
                    self.garbage.remove(lnew)
                    self.wght[lnew] = 0

                if lnew not in self.wght:
                    self.set_bound(t, b)
                else:
                    self.wght[lnew] += self.minw
            elif not t: # ss totalizer
                lnew = -self.ssenc.get_output(-l, b)
                if lnew:
                    if lnew in self.garbage:
                        self.garbage.remove(lnew)
                        self.wght[lnew] = 0
                    if lnew not in self.wght:
                        self.wght[lnew]=self.minw
                        self.bnds[lnew]=b
                        self.sums.append(lnew)
                    else:
                        self.wght[lnew]+=self.minw
                


    def create_sum(self, bound=1):
        """
            Create a totalizer object encoding a cardinality
            constraint on the new list of relaxation literals obtained
            in :func:`process_sels` and :func:`process_sums`. The
            clauses encoding the sum of the relaxation literals are
            added to the SAT oracle. The sum of the totalizer object
            is encoded up to the value of the input parameter
            ``bound``, which is set to ``1`` by default.

            :param bound: right-hand side for the sum to be created
            :type bound: int

            :rtype: :class:`.ITotalizer`

            Note that if Minicard is used as a SAT oracle, native
            cardinality constraints are used instead of
            :class:`.ITotalizer`.
        """

        if self.solver not in SolverNames.minicard:  # standard totalizer-based encoding
            # new totalizer sum
            if self.lazy_left:
                active_rels=[i for i in self.rels if -i in self.garbage or -i in self.sels_to_deactivate or -i in self.sums_to_deactivate]
                lazy_rels=[i for i in self.rels if -i not in self.garbage and -i not in self.sels_to_deactivate and -i not in self.sums_to_deactivate]

                t = ITotalizerLazier(lits=active_rels, ubound=bound, top_id=self.topv, actual_size=len(self.rels))
                for i in lazy_rels:
                    if -i not in self.tot_lazy_inputs: self.tot_lazy_inputs[-i]=[]
                    self.tot_lazy_inputs[-i].append(t)
            else:
                t = ITotalizer(lits=self.rels, ubound=bound, top_id=self.topv)

            # updating top variable id
            self.topv = t.top_id

            # adding its clauses to oracle
            for cl in t.cnf.clauses:
                self.oracle.add_clause(cl)

            if self.solver in SolverNames.glucose3s:
                ivars=set()
                for cl in t.cnf.clauses:
                    for l in cl:
                        ivars.add(abs(l))
                for l in self.rels:
                    if abs(l) in ivars: ivars.remove(abs(l)) 
                self.stats_sum_variables[t]=list(ivars)
        else:
            # for minicard, use native cardinality constraints instead of the
            # standard totalizer, i.e. create a new (empty) totalizer sum and
            # fill it with the necessary data supported by minicard
            t = ITotalizer()
            t.lits = self.rels

            self.topv += 1  # a new variable will represent the bound

            # proper initial bound
            t.rhs = [None] * (len(t.lits))
            t.rhs[bound] = self.topv

            # new atmostb constraint instrumented with
            # an implication and represented natively
            rhs = len(t.lits)
            amb = [[-self.topv] * (rhs - bound) + t.lits, rhs]

            # add constraint to the solver
            self.oracle.add_atmost(*amb)

        return t

    def update_sum(self, assump):
        """
            The method is used to increase the bound for a given
            totalizer sum. The totalizer object is identified by the
            input parameter ``assump``, which is an assumption literal
            associated with the totalizer object.

            The method increases the bound for the totalizer sum,
            which involves adding the corresponding new clauses to the
            internal SAT oracle.

            The method returns the totalizer object followed by the
            new bound obtained.

            :param assump: assumption literal associated with the sum
            :type assump: int

            :rtype: :class:`.ITotalizer`, int

            Note that if Minicard is used as a SAT oracle, native
            cardinality constraints are used instead of
            :class:`.ITotalizer`.
        """

        if assump in self.tobj:
            # getting a totalizer object corresponding to assumption
            t = self.tobj[assump]

            # increment the current bound
            b = self.bnds[assump] + 1

            if self.solver not in SolverNames.minicard:  # the case of standard totalizer encoding
                # increasing its bound
                t.increase(ubound=b, top_id=self.topv)

                # updating top variable id
                self.topv = t.top_id

                # adding its clauses to oracle
                if t.nof_new:
                    for cl in t.cnf.clauses[-t.nof_new:]:
                        self.oracle.add_clause(cl)
                    
                    if self.solver in SolverNames.glucose3s:
                        ivars=set()
                        for cl in t.cnf.clauses:
                            for l in cl:
                                ivars.add(abs(l))
                        for l in self.stats_sum_variables[t]:
                            if abs(l) in ivars: ivars.remove(abs(l)) 
                        self.stats_sum_variables[t].extend(list(ivars))
            else:  # the case of cardinality constraints represented natively
                # right-hand side is always equal to the number of input literals
                rhs = len(t.lits)

                if b < rhs:
                    # creating an additional bound
                    if not t.rhs[b]:
                        self.topv += 1
                        t.rhs[b] = self.topv

                    # a new at-most-b constraint
                    amb = [[-t.rhs[b]] * (rhs - b) + t.lits, rhs]
                    self.oracle.add_atmost(*amb)
            return t, b
        else: #ss totalizer
            b = self.bnds[assump] + 1
            self.ssenc.prepare_next_output(-assump, self.topv)
            self.topv=self.ssenc.top_id
            if self.ssenc.nof_new:
                for cl in self.ssenc.cnf.clauses[-self.ssenc.nof_new:]:
                    self.oracle.add_clause(cl)
            return None, b
            
            
    def set_bound(self, tobj, rhs):
        """
            Given a totalizer sum and its right-hand side to be
            enforced, the method creates a new sum assumption literal,
            which will be used in the following SAT oracle calls.

            :param tobj: totalizer sum
            :param rhs: right-hand side

            :type tobj: :class:`.ITotalizer`
            :type rhs: int
        """

        # saving the sum and its weight in a mapping
        self.tobj[-tobj.rhs[rhs]] = tobj
        self.bnds[-tobj.rhs[rhs]] = rhs
        self.wght[-tobj.rhs[rhs]] = self.minw

        # adding a new assumption to force the sum to be at most rhs
        self.sums.append(-tobj.rhs[rhs])


    def activate_lazy_inputs(self):
        """
            When totalizer left-hand-side is built lazily, this function is
            called to add new active lits to totalizers
        """
        for t in self.tot_new_inputs:
            t.activate_lits(self.tot_new_inputs[t], self.topv)
            self.topv=t.top_id
            if t.nof_new:
                for cl in t.cnf.clauses[-t.nof_new:]:  self.oracle.add_clause(cl)

    def activate_lazy_input(self, l):
        """
            When totalizer left-hand side is built lazily, this function is called
            for every lit l that is to be removed from the assumptions.
            After that, activate_lazy_inputs is called.
        """
        if l in self.tot_lazy_inputs:
            for t in self.tot_lazy_inputs[l]:
                if t not in self.tot_new_inputs: self.tot_new_inputs[t]=[]
                self.tot_new_inputs[t].append(-l)
            self.tot_lazy_inputs[l][:]=[]

    def filter_assumps(self):
        """
            Filter out unnecessary selectors and sums from the list of
            assumption literals. The corresponding values are also
            removed from the dictionaries of bounds and weights.

            Note that assumptions marked as garbage are collected in
            the core processing methods, i.e. in :func:`process_core`,
            :func:`process_sels`, and :func:`process_sums`.
        """
        if self.lazy_left:
            self.tot_new_inputs={}
            self.pmres_new_inputs={}
            for l in self.garbage: self.activate_lazy_input(l)
            for l in self.sums_to_deactivate: self.activate_lazy_input(l)
            for l in self.sels_to_deactivate: self.activate_lazy_input(l)
            self.activate_lazy_inputs()

        self.sels = list(filter(lambda x: x not in self.garbage, self.sels))
        self.sums = list(filter(lambda x: x not in self.garbage, self.sums))

        self.bnds = {l: b for l, b in six.iteritems(self.bnds) if l not in self.garbage}
        self.wght = {l: w for l, w in six.iteritems(self.wght) if l not in self.garbage}

        self.sels_set.difference_update(set(self.garbage))

        self.garbage.clear()

    def oracle_time(self):
        """
            Report the total SAT solving time.
        """
        return self.oracle.time_accum()

    def _map_extlit(self, l):
        """
            Map an external variable to an internal one if necessary.

            This method is used when new clauses are added to the
            formula incrementally, which may result in introducing new
            variables clashing with the previously used *clause
            selectors*. The method makes sure no clash occurs, i.e. it
            maps the original variables used in the new problem
            clauses to the newly introduced auxiliary variables (see
            :func:`add_clause`).

            Given an integer literal, a fresh literal is returned. The
            returned integer has the same sign as the input literal.

            :param l: literal to map
            :type l: int

            :rtype: int
        """
        v = abs(l)

        if v in self.vmap.e2i:
            return int(copysign(self.vmap.e2i[v], l))
        else:
            self.topv += 1

            self.vmap.e2i[v] = self.topv
            self.vmap.i2e[self.topv] = v

            return int(copysign(self.topv, l))

    def print_stats(self):
        b="c SOLVER-STATS"
        if self.use_ss:
            print(b, "ssenc_relax_time:", self.ssenc._time_relax)
            print(b, "ssenc_next_time:", self.ssenc._time_next)
            self.ssenc.print_stats(b+" ")
        print(b, "relax_wcores_time:", self._time_wcores)
        print(b, "satsolver_time:", self._time_satsolver)
        if self.oracle:
            self.oracle.print_stats()
            
            if self.solver in SolverNames.glucose3s:
                learnts, propagations = self.oracle.get_lstats()

                in_clauses={}
                nodemaps, ps={}, {}
                for i in self.stats_sums:
                    nodemaps[i], ps[i]=count.reconstruct(i.cnf.clauses, i.lits)
                    for l in self.stats_sum_variables[i]:
                        if l not in in_clauses:
                            in_clauses[l], in_clauses[-l]=[], []
                        in_clauses[l].append(i)
                        in_clauses[-l].append(i)

                total_possibilities=0
                for cl in learnts:
                    beta=set()
                    first=1
                    for l in cl:
                        vl=in_clauses.get(l, [])
                        if len(vl):
                            if first:
                                for i in vl: beta.add(i)
                                first=0
                            else:
                                rm=[k for k in beta if k not in vl]
                                for i in rm: beta.remove(i)
                    if len(beta)!=1: continue
                    clause_as_set=set(cl)
                    must=set()
                    for l in cl:  must.add(-l)
                    for sum_o in beta:
                        rv=count.count(cl, clause_as_set, must, nodemaps[sum_o], ps[sum_o])
                        total_possibilities+=rv
                        
                print(b, "useful_learning_estimate:", total_possibilities)

        print(b, "iters:", self.stats_iters)
        print(b, "cores:", self.stats_cores)
        print(b, "coresizes[]:", self.stats_coresizes)
        print(b, "wce_coresizes[][]:", self.stats_wce_coresizes)
        print(b, "coresize_sum:", self.stats_coresize_sum)
        print(b, "assumpssizes[]:", self.stats_assumpssizes)
        print(b, "wce_assumpssizes[]:", self.stats_wce_assumpssizes)
        print(b, "wce_cores[]:", self.stats_wce_cores)
        print(b, "assumpssize_sum:", self.stats_assumpssize_sum)
        print(b, "sums_rhss[]:", [len(i.rhs) for i in self.stats_sums])
        if self.lazy_left:
            print(b, "sums_initial_sizes[]:", [i.initial_size for i in self.stats_sums])
            print(b, "sums_final_sizes[]:", [len(i.lits) for i in self.stats_sums])
            print(b, "sums_nof_lits[]:", [i.actual_size for i in self.stats_sums])
        else:
            print(b, "sums_nof_lits[]:", [len(i.lits) for i in self.stats_sums])

#
#==============================================================================
class RC2WCEStratified(RC2WCE, object):
    """
        RC2 augmented with BLO and stratification techniques. Although
        class :class:`RC2` can deal with weighted formulas, there are
        situations when it is necessary to apply additional heuristics
        to improve the performance of the solver on weighted MaxSAT
        formulas. This class extends capabilities of :class:`RC2` with
        two heuristics, namely

        1. Boolean lexicographic optimization (BLO) [5]_
        2. stratification [6]_

        There is no way to enable only one of them -- both heuristics
        are applied at the same time. Except for the aforementioned
        additional techniques, every other component of the solver
        remains as in the base class :class:`RC2`. Therefore, a user
        is referred to the documentation of :class:`RC2` for details.
    """

    def __init__(self, formula, solver='g3', adapt=False, fix_dec_order=0, group_softs=0, exhaust=False, incr=False, minz=False,
            structure_sharing_opts=(1, 8), trim=0, verbose=0, add_cores=0, disable_learning=False, pbc_opts=3, use_lower_bounds=False, use_upper_bounds=False,
            lazy_left=False, sdivp=2.0, strat_lower_bounds=False, speed_up_assumps=0, shuffle_core=False, deactivate=True, pmres=False):
        """
            Constructor.
        """

        # calling the constructor for the basic version
        super(RC2WCEStratified, self).__init__(formula, solver=solver, adapt=adapt, fix_dec_order=fix_dec_order, group_softs=group_softs, exhaust=exhaust,
                incr=incr, minz=minz, structure_sharing_opts=structure_sharing_opts, trim=trim, verbose=verbose, add_cores=add_cores, disable_learning=disable_learning,
                pbc_opts=pbc_opts, use_lower_bounds=use_lower_bounds,  use_upper_bounds=use_upper_bounds, lazy_left=lazy_left, sdivp=sdivp, strat_lower_bounds=strat_lower_bounds, 
                speed_up_assumps=speed_up_assumps, shuffle_core=shuffle_core, deactivate=deactivate, pmres=pmres)

        self.levl = 0   # initial optimization level
        self.blop = []  # a list of blo levels

        # backing up selectors
        self.bckp, self.bckp_set = self.sels, self.sels_set
        self.sels = []

        # initialize Boolean lexicographic optimization
        self.init_wstr()

    def init_wstr(self):
        """
            Compute and initialize optimization levels for BLO and
            stratification. This method is invoked once, from the
            constructor of an object of :class:`RC2Stratified`. Given
            the weights of the soft clauses, the method divides the
            MaxSAT problem into several optimization levels.
        """

        # a mapping for stratified problem solving,
        # i.e. from a weight to a list of selectors
        self.wstr = collections.defaultdict(lambda: [])

        for s, w in six.iteritems(self.wght):
            self.wstr[w].append(s)

        # sorted list of distinct weight levels
        self.blop = sorted([w for w in self.wstr], reverse=True)

        # diversity parameter for stratification
        self.sdiv = len(self.blop) / sdivp

    def compute(self):
        """
            This method solves the MaxSAT problem iteratively. Each
            optimization level is tackled the standard way, i.e. by
            calling :func:`compute_`. A new level is started by
            calling :func:`next_level` and finished by calling
            :func:`finish_level`. Each new optimization level
            activates more soft clauses by invoking
            :func:`activate_clauses`.
        """
        done = 0  # levels done

        # first attempt to get an optimization level
        self.next_level()

        while self.levl != None and done < len(self.blop):
            # add more clauses
            done = self.activate_clauses(done)

            if self.verbose > 1:
                print('c wght str:', self.blop[self.levl])

            if self.compute_() == False:
                return

            # updating the list of distinct weight levels
            self.blop = sorted([w for w in self.wstr], reverse=True)

            if done < len(self.blop):
                if self.verbose > 1:
                    print('c curr opt:', self.cost)

                # done with this level
                self.finish_level()

                self.levl += 1

                # get another level
                self.next_level()

                if self.verbose > 1:
                    print('c')

        # extracting a model
        if self.cost==self.UB:
            self.model=self.best_model
        else:
            self.model = self.oracle.get_model()
    
        self.model = filter(lambda l: abs(l) in self.vmap.i2e, self.model)
        self.model = map(lambda l: int(copysign(self.vmap.i2e[abs(l)], l)), self.model)
        self.model = sorted(self.model, key=lambda l: abs(l))

        return self.model

    def next_level(self):
        """
            Compute the next optimization level (starting from the
            current one). The procedure represents a loop, each
            iteration of which checks whether or not one of the
            conditions holds:

            - partial BLO condition
            - stratification condition

            If any of these holds, the loop stops.
        """
        if self.levl >= len(self.blop):
            self.levl = None
            return

        while self.levl < len(self.blop) - 1:
            # number of selectors with weight less than current weight
            numc = sum([len(self.wstr[w]) for w in self.blop[(self.levl + 1):]])

            # sum of their weights
            sumw = sum([w * len(self.wstr[w]) for w in self.blop[(self.levl + 1):]])

            # partial BLO
            if self.blop[self.levl] > sumw and sumw != 0:
                break

            # stratification
            if numc / float(len(self.blop) - self.levl - 1) > self.sdiv:
                break

            self.levl += 1

    def activate_clauses(self, beg):
        """
            This method is used for activating the clauses that belong
            to optimization levels up to the newly computed level. It
            also reactivates previously deactivated clauses (see
            :func:`process_sels` and :func:`process_sums` for
            details).
        """
        end = min(self.levl + 1, len(self.blop))

        for l in range(beg, end):
            for sel in self.wstr[self.blop[l]]:
                if sel in self.bckp_set:
                    self.sels.append(sel)
                else:
                    self.sums.append(sel)

        # updating set of selectors
        self.sels_set = set(self.sels)

        return end

    def finish_level(self):
        """
            This method does postprocessing of the current
            optimization level after it is solved. This includes
            *hardening* some of the soft clauses (depending on their
            remaining weights) and also garbage collection.
        """
        # assumptions to remove
        self.garbage = set()

        # sum of weights of the remaining levels
        sumw = sum([w * len(self.wstr[w]) for w in self.blop[(self.levl + 1):]])

        # trying to harden selectors and sums
        for s in self.sels + self.sums:
            if self.wght[s] > sumw:
                self.oracle.add_clause([s])
                self.garbage.add(s)

        if self.verbose > 1:
            print('c hardened:', len(self.garbage))

        # remove unnecessary assumptions
        self.filter_assumps()

        if not self.deactivate_lits: # normal deactivation is disabled, deactivate when level is finished
            # selectors that should be deactivated (but not removed completely)
            self.sels_to_deactivate = set()
            for s in self.sels:
                if self.wght[s] < self.blop[self.levl]:
                    self.wstr[self.wght[s]].append(s)
                    self.sels_to_deactivate.add(s)
            self.sels = list(filter(lambda x: x not in self.sels_to_deactivate, self.sels))
            self.sums_to_deactivate = set()
            for s in self.sels:
                if self.wght[s] < self.blop[self.levl]:
                    self.wstr[self.wght[s]].append(s)
                    self.sums_to_deactivate.add(s)
            self.sums = list(filter(lambda x: x not in self.sums_to_deactivate, self.sums))
        

    def process_am1(self, am1):
        """
            Due to the solving process involving multiple optimization
            levels to be treated individually, new soft clauses for
            the detected intrinsic AtMost1 constraints should be
            remembered. The method is a slightly modified version of
            the base method :func:`RC2.process_am1` taking care of
            this.
        """
        # computing am1's weight
        self.minw = min(map(lambda l: self.wght[l], am1))

        # pretending am1 to be a core, and the bound is its size - 1
        self.core_sels, b = am1, len(am1) - 1

        # incrementing the cost
        self.cost += b * self.minw

        # assumptions to remove
        self.garbage = set()

        # splitting and relaxing if needed
        self.process_sels()

        # new selector
        self.topv += 1
        selv = self.topv

        self.oracle.add_clause([-l for l in self.rels] + [-selv])

        # integrating the new selector
        self.sels.append(selv)
        self.wght[selv] = self.minw
        self.smap[selv] = len(self.wght) - 1

        # do not forget this newly selector!
        self.bckp_set.add(selv)

        # removing unnecessary assumptions
        self.filter_assumps()

    def process_sels(self):
        """
            A redefined version of :func:`RC2.process_sels`. The only
            modification affects the clauses whose weight after
            splitting becomes less than the weight of the current
            optimization level. Such clauses are deactivated and to be
            reactivated at a later stage.
        """

        # new relaxation variables
        self.rels = []

        # selectors that should be deactivated (but not removed completely)
        self.sels_to_deactivate = set()

        for l in self.core_sels:
            self.in_core(l)
            if self.wght[l] == self.minw:
                # marking variable as being a part of the core
                # so that next time it is not used as an assump
                self.garbage.add(l)

                self.rels.append(-l)
            else:
                # do not remove this variable from assumps
                # since it has a remaining non-zero weight
                self.wght[l] -= self.minw

                # deactivate this assumption and put at a lower level
                if self.wght[l] < self.blop[self.levl] and self.deactivate_lits:
                    self.wstr[self.wght[l]].append(l)
                    self.sels_to_deactivate.add(l)

                self.rels.append(-l)
        # deactivating unnecessary selectors
        self.sels = list(filter(lambda x: x not in self.sels_to_deactivate, self.sels))

    def process_sums(self):
        """
            A redefined version of :func:`RC2.process_sums`. The only
            modification affects the clauses whose weight after
            splitting becomes less than the weight of the current
            optimization level. Such clauses are deactivated and to be
            reactivated at a later stage.
        """

        # sums that should be deactivated (but not removed completely)
        self.sums_to_deactivate = set()

        for l in self.core_sums:
            if self.wght[l] == self.minw:
                # marking variable as being a part of the core
                # so that next time it is not used as an assump
                self.garbage.add(l)
            else:
                # do not remove this variable from assumps
                # since it has a remaining non-zero weight
                self.wght[l] -= self.minw

                # deactivate this assumption and put at a lower level
                if self.wght[l] < self.blop[self.levl] and self.deactivate_lits:
                    self.wstr[self.wght[l]].append(l)
                    self.sums_to_deactivate.add(l)
            # put this assumption to relaxation vars
            self.rels.append(-l)
            if self.pmres: continue
            # increase bound for the sum
            t, b = self.update_sum(l)

            # updating bounds and weights
            if t and b < len(t.rhs): #normal totalizer
                lnew = -t.rhs[b]
                if lnew in self.garbage:
                    self.garbage.remove(lnew)
                    self.wght[lnew] = 0

                if lnew not in self.wght:
                    self.set_bound(t, b)
                else:
                    self.wght[lnew] += self.minw
            elif not t: # ss totalizer
                lnew = -self.ssenc.get_output(-l, b)
                if lnew:
                    if lnew in self.garbage:
                        self.garbage.remove(lnew)
                        self.wght[lnew] = 0
                    if lnew not in self.wght:
                        self.wght[lnew] = self.minw
                        self.bnds[lnew] = b
                        self.sums.append(lnew)
                    else:
                        self.wght[lnew] += self.minw


        # deactivating unnecessary sums
        self.sums = list(filter(lambda x: x not in self.sums_to_deactivate, self.sums))


#
#==============================================================================
def parse_options():
    """
        Parses command-line option
    """
    try:
        opts, args = getopt.getopt(sys.argv[1:], 'abc:Cd:De:f:g:hilLmMno:pPrRs:S:t:T:uvV:xz',
                ['adapt', 'block-mcses', 'comp=', 'add-cores=', 'disable-deactivation', 'disable-learning', 'enum=', 'fix-dec-order=', 'group-softs=', 'exhaust', 'help',
                    'incr', 'lower-bounds', 'blo', 'minimize', 'maxpre', 'no-structure-sharing', 'print-stats', 'PMRES', 'pbc-opts=', 'random-rel-order', 
                    'sdiv=', 'strat-lb', 'solver=', 'speed-up-assumps=', 'trim=', 'ss-options=', 'use-ub', 'verbose', 'vnew', 'lazy-left'])
    except getopt.GetoptError as err:
        sys.stderr.write(str(err).capitalize())
        usage()
        sys.exit(1)

    adapt = False
    block_mcses = False
    exhaust = False
    cmode = None
    deactivate=1
    to_enum = 1
    fix_dec_order=0
    group_softs = 0
    incr = False
    blo = False
    minz = False
    maxpre = False
    structure_sharing_opts = (1, 16)
    sdivp = 2.0
    solver = 'g3'
    speed_up_assumps_strategy = 0
    trim = 0
    verbose = 1
    vnew = False
    add_cores = 0
    disable_learning = False
    pmres=False
    pbc_opts = 3
    use_lb = False
    use_ub = False
    shuffle_core = False
    strat_lb = False
    print_stats = False
    lazy_left = False

    for opt, arg in opts:
        if opt in ('-a', '--adapt'):
            adapt = True
        elif opt in ('-b', '--block-mcses'):
            block_mcses = True
        elif opt in ('-c', '--comp'):
            cmode = str(arg)
        elif opt in ('-C', '--disable-deactivation'):
            deactivate = 0
        elif opt in ('-d', '--add-cores'):
            add_cores = int(arg)
        elif opt in ('-D', '--disable-learning'):
            disable_learning = 1
        elif opt in ('-e', '--enum'):
            to_enum = str(arg)
            if to_enum != 'all':
                to_enum = int(to_enum)
            else:
                to_enum = 0
        elif opt in ('-f', '--fix-dec-order'):
            fix_dec_order=int(arg)
        elif opt in ('-g', '--group-softs'):
            group_softs = int(arg)
        elif opt in ('-h', '--help'):
            usage()
            sys.exit(0)
        elif opt in ('-i', '--incr'):
            incr = True
        elif opt in ('-l', '--blo'):
            blo = True
        elif opt in ('-L', '--lower-bounds'):
            use_lb = True
        elif opt in ('-m', '--minimize'):
            minz = True
        elif opt in ('-M', '--maxpre'):
            maxpre = True
        elif opt in ('-n', '--no-structure-sharing'):
            structure_sharing_opts = None
        elif opt in ('-p', '--print-stats'):
            print_stats = True
        elif opt in ('-P', '--PMRES'):
            pmres = True
        elif opt in ('-o', '--pbc-opts'):
            pbc_opts = int(arg)
        elif opt in ('-r', '--strat-lb'):
            strat_lb = True
        elif opt in ('-R', '--random-rel-order'):
            shuffle_core = True
        elif opt in ('-s', '--solver'):
            solver = str(arg)
        elif opt in ('-S', '--speed-up-assumps'):
            speed_up_assumps_strategy=int(arg)
        elif opt in ('-t', '--trim'):
            trim = int(arg)
        elif opt in ('-T', '--ss-options'):
            structure_sharing_opts=(int(arg.split(",")[0]), int(arg.split(",")[1]))
        elif opt in ('-u', '--use-ub'):
            use_ub=True
        elif opt in ('-v', '--verbose'):
            verbose += 1
        elif opt in ('-V', '--sdiv'):
            sdivp = float(arg)
        elif opt == '--vnew':
            vnew = True
        elif opt in ('-x', '--exhaust'):
            exhaust = True
        elif opt in ('-z', '--lazy-left'):
            lazy_left = True
        else:
            assert False, 'Unhandled option: {0} {1}'.format(opt, arg)

    return adapt, blo, block_mcses, cmode, deactivate, to_enum, fix_dec_order, group_softs, exhaust, incr, minz, maxpre, structure_sharing_opts, solver, speed_up_assumps_strategy, trim, \
             verbose, vnew, add_cores, disable_learning, pbc_opts, use_lb, sdivp, strat_lb, print_stats, lazy_left, shuffle_core, pmres, use_ub, args


#
#==============================================================================
def usage():
    """
        Prints usage message.
    """
    print('Usage:', os.path.basename(sys.argv[0]), '[options] dimacs-file')
    print('Options:')
    print('        -a, --adapt              Try to adapt (simplify) input formula')
    print('        -b, --block-mcses        When enumerating MaxSAT models, block entire MCSes instead of models')
    print('        -c, --comp=<string>      Enable one of the MSE18 configurations')
    print('                                 Available values: a, b, none (default = none)')
    print('        -C, --disable-deactivation Don\'t deactivate lits when their weight drops below stratification bound')
    print('        -d, --add-cores=<int>    Add found cores as hard clauses in the solver if core size is smaller than given constant')
    print('        -D, --disable-learning   Disable SAT-solvers learning.')
    print('        -e, --enum=<int>         Number of MaxSAT models to compute')
    print('                                 Available values: [1 .. INT_MAX], all (default = 1)')
    print('        -f, --fix-dec-order=<int>Fix SAT-solvers decision order: 1 smallest var to largest, -1 largest var to smallest')
    print('        -g, --group-softs        Run algorithm to detect groups of exchangeable soft variables')
    print('        -h, --help               Show this message')
    print('        -i, --incr               Use SAT solver incrementally (only for g3 and g4)')
    print('        -l, --blo                Use BLO and stratification')
    print('        -L, --lower-bounds       Use lower bounds (minisatpbc only)')
    print('        -m, --minimize           Use a heuristic unsatisfiable core minimizer')
    print('        -M, --maxpre             Use maxpre preprocessing.')
    print('        -n, --no-structure-sharing  Don\'t use structure sharing technique')
    print('        -s, --solver=<string>    SAT solver to use')
    print('                                 Available values: g3, g4, lgl, mcb, mcm, mpl, m22, mc, mgh (default = g3)')
    print('        -S, --speed-up-assumps=<int> set speed up assumption based sat strategy: 1 paper, 2 ')
    print('        -o, --pbc-opts=<int>     Options set to minisatpbc solver')
    print('        -p, --stats              Print solver stats')
    print('        -P, --PMRES              Use PMRES relaxing instead of OLL')
    print('        -r, --strat-lb           When using lower bounds, put only those lits in pbc that have been in a core')
    print('        -R, --random-rel-order   Shuffle lits in core when relaxing it')
    print('        -t, --trim=<int>         How many times to trim unsatisfiable cores')
    print('                                 Available values: [0 .. INT_MAX] (default = 0)')
    print('        -T, --ss-options=(<int>,<int>)   Options for structure sharing: reusing, thresold (default: 1,16)')
    print('        -u, --use-ub             Use upper bounds to optimize search')
    print('        -v, --verbose            Be verbose')
    print('        -V, --sdiv=<float>       Adjust diversity parameter for stratification (default: 2.0)')
    print('        --vnew                   Print v-line in the new format')
    print('        -x, --exhaust            Exhaust new unsatisfiable cores')
    print('        -z, --lazy-left          Build also totalizers left-hand side lazily')


#
#==============================================================================
if __name__ == '__main__':
    adapt, blo, block_mcses, cmode, deactivate, to_enum, fix_dec_order, group_softs, exhaust, incr, minz, maxpre, ss_opts, solver, speed_up_assumps, trim, verbose, \
    vnew, add_cores, disable_learning, pbc_opts, use_lb, sdivp, strat_lb, print_stats, lazy_left, shuffle_core, pmres, use_ub, files = parse_options()
    if files:
        # parsing the input formula
        if re.search('\.wcnf[p|+]?(\.(gz|bz2|lzma|xz))?$', files[0]):
            formula = WCNFPlus(from_file=files[0])
        else:  # expecting '*.cnf[,p,+].*'
            formula = CNFPlus(from_file=files[0]).weighted()

        if maxpre:
            formula=preprocess_formula(formula)

        # enabling the competition mode
        if cmode:
            assert cmode in ('a', 'b'), 'Wrong MSE18 mode chosen: {0}'.format(cmode)
            adapt, blo, exhaust, solver, verbose = True, True, True, 'g3', 3

            if cmode == 'a':
                trim = 5 if max(formula.wght) > min(formula.wght) else 0
                minz = False
            else:
                trim, minz = 0, True

            # trying to use unbuffered standard output
            if sys.version_info.major == 2:
                sys.stdout = os.fdopen(sys.stdout.fileno(), 'w', 0)

        # deciding whether or not to stratify
        if blo and max(formula.wght) > min(formula.wght):
            assert to_enum == 1, 'Stratified solving is incompatible with model enumeration'
            MXS = RC2WCEStratified
        else:
            MXS = RC2WCE


t = IGENTotalizer(lits=[1, 2, 3, 4], weights=[2,3,5,7], ubound=11)
print(t.cnf.clauses)
print(t.rhs)
print("Testing")
s = Solver(name=solver, bootstrap_with=t.cnf.clauses, incr=incr, use_timer=True)

b = s.solve()

print(s.get_model())
print("B {:n}".format(b))

s.delete()
t.delete()


        # starting the solver
        
#        with MXS(formula, solver=solver, adapt=adapt, fix_dec_order=fix_dec_order, group_softs=group_softs, exhaust=exhaust, incr=incr, minz=minz, 
#                structure_sharing_opts=ss_opts, trim=trim, verbose=verbose, add_cores=add_cores, disable_learning=disable_learning, pbc_opts=pbc_opts, use_lower_bounds=use_lb,  use_upper_bounds=use_ub,
#                lazy_left=lazy_left, sdivp=sdivp, strat_lower_bounds=strat_lb, speed_up_assumps=speed_up_assumps, shuffle_core=shuffle_core, pmres=pmres, deactivate=deactivate,) as rc2:
#
#            optimum_found = False
#            for i, model in enumerate(rc2.enumerate(block_mcses=block_mcses), 1):
#                optimum_found = True
#
#                if verbose:
#                    if print_stats:
#                        rc2.print_stats()
#                    if i == 1:
#                        print('s OPTIMUM FOUND')
#                        print('o {0}'.format(rc2.cost))
#
#                    if verbose > 2:
#                        if vnew:  # new format of the v-line
#                            print('v', ''.join(str(int(l > 0)) for l in model))
#                        else:
#                            print('v', ' '.join([str(l) for l in model]))
#
#                if i == to_enum:
#                    break
#
#            # needed for MSE'20
#            if to_enum != 1 and block_mcses:
#                print('v')
#
#            if verbose:
#                if not optimum_found:
#                    print('s UNSATISFIABLE')
#                elif to_enum != 1:
#                    print('c models found:', i)

#                if verbose > 1:
#                    print('c oracle time: {0:.4f}'.format(rc2.oracle_time()))
