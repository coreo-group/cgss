CGSS, a core guided Max-SAT-algorithm using structure sharing technique for enhanced cardinality constraints, built on RC2 and PySAT
====================================================================================================================================

This repository extends on RC2 and PySAT with refined core relaxation techniques that improve the performance
of MaxSAT-solving:

-  Weight aware core extraction (WCE)
-  Cardinality constraints enhanced with structure sharing (SS)

There is also an implementation of the PMRES algorithm, extended with structure sharing.

If you use CGSS in your work, please cite:
Hannes Ihalainen, Jeremias Berg, and Matti Järvisalo. Refined Core Relaxation for Core-Guided MaxSAT
Solving. In International Conference on Principles and Practice of Constraint Programming LIPIcs. 
Schloss Dagstuhl – Leibniz-Zentrum für Informatik, 2021. Volume 210, pages: 28:1--28:19


@inproceedings{DBLP:conf/cp/IhalainenBJ21,
  author    = {Hannes Ihalainen and
               Jeremias Berg and
               Matti J{\"{a}}rvisalo},
  title     = {Refined Core Relaxation for Core-Guided MaxSAT Solving},
  booktitle = {{CP}},
  series    = {LIPIcs},
  volume    = {210},
  pages     = {28:1--28:19},
  publisher = {Schloss Dagstuhl - Leibniz-Zentrum f{\"{u}}r Informatik},
  year      = {2021}
}

Installation

.. code:: bash

    python setup.py install

Running RC2*

.. code:: bash

    cd examples
    python rc2.py -lxamWnN [instance.wcnf.gz]

Running RC2*+WCE

.. code:: bash

    cd examples
    python rc2.py -lxamWn [instance.wcnf.gz]

Running RC2*+WCE+SS

.. code:: bash

    cd examples
    python rc2.py -lxamW -T 1,16 -E 50,50 [instance.wcnf.gz]


Running PMRES

.. code:: bash

    cd examples
    python rc2.py -lamWnNP [instance.wcnf.gz]

Running PMRES+WCE

.. code:: bash

    cd examples
    python rc2.py -lamWnP [instance.wcnf.gz]

Running PMRES+WCE+SS

.. code:: bash

    cd examples
    python rc2.py -lamWP -T 1,16 [instance.wcnf.gz]

Parameters on the examples explained:

.. code::

    -l enable BLO and stratification
    -x enable core exhaustion
    -a enable AtMost1-constraint simplification
    -m enable core minimization
    -W enable WCE-version, enables also structure sharing
    -N disable WCE technique, use together with -W to use WCE+SS code base but without WCE technique
    -n disable structure sharing, use together with -W
    -T <int,int> set parameters used in structure sharing: <reuse, min_set_size>
    -E <int,int> set parameters for structure sharing equivalence technique: <thresold, max_cost>
    -P use PMRES instead of OLL



More about possible parameters:

.. code:: bash

    cd examples
    python rc2.py -h



As CGSS is built on top of PySAT, it supports most of the features that PYSAT does. 
For more info and the rest of the PySAT readme, please see 
<https://pysathq.github.io/>


License
-------

PySAT is licensed under `MIT <LICENSE.txt>`__.
