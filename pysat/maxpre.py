try:
    import pymaxpre
except Exception:
    pass
from pysat.formula import WCNFPlus

def preprocess(clauses, weights, topw, timelimit, loglevel, options):
    return pymaxpre.preprocess(clauses, weights, topw, timelimit, loglevel, options)



def preprocess_formula(formula, timelimit=1000, options="[bu]#[buvsrgc]"):
    clauses=formula.soft+formula.hard
    weights=formula.wght+([formula.topw]*len(formula.hard))
    topw=formula.topw
    processor, nclauses, nweights=preprocess(clauses, weights, topw, timelimit, 0, options)

    processed_formula=WCNFPlus()
    for i in range(len(nweights)):
        if nweights[i]<topw:       processed_formula.append(nclauses[i], weight=nweights[i])
        else:                      processed_formula.append(nclauses[i])

    return processed_formula
